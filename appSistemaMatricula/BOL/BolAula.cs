﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sistemaMatricula.Repositorios;
using sistemaMatricula.Entidades;

namespace appSistemaMatricula.BOL
{
    public class BolAula
    {
        private RepAula repAula;

        public BolAula()
        {
            this.repAula = new RepAula();
        }
        /// <summary>
        /// Trae una lista de aulas de la base de datos
        /// </summary>
        /// <returns>Arreglo de Aulas</returns>
        internal List<Aula> ObtenerAulas()
        {
            List<Aula> aulas = new List<Aula>();
            try
            {
                aulas = this.repAula.ObtenerAulas();
            }
            catch (Exception ex)
            {
                new Exception("Error al cargar las Aulas : " + ex.Message);
            }
            return aulas;
        }
        /// <summary>
        /// Valida los criterios de búsqueda para obtener un Aula
        /// Solicitando un objeto de este tipo para determinar los criterios de búsqueda
        /// </summary>
        /// <param name="oAula">Entidad Aula</param>
        /// <returns>Entidad Aula</returns>
        internal List<Aula> BuscarAulas(Aula oAula)
        {
            List<Aula> aulas;
            try
            {
                if (oAula.Numero == 0 && String.IsNullOrEmpty(oAula.Descripcion))
                {
                    throw new Exception("Criterio de búsqueda erróneo");
                }
                aulas = this.repAula.BuscarAulas(oAula);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al consultar Aula: " + ex.Message);
            }
            return aulas;
        }
        /// <summary>
        /// Guarda una Entidad Aula en la base de datos
        /// Valida que el objeto no traiga datos erróneos o nulos
        /// </summary>
        /// <param name="oAula"></param>
        /// <returns>True = Creada, False = No creada</returns>
        internal bool CrearAula(Aula oAula)
        {
            bool creada = false;
            try
            {
                if (String.IsNullOrEmpty(oAula.Descripcion) || oAula.Numero == 0)
                {
                    throw new Exception("Datos del aula incompletos");
                }
                if (this.repAula.CrearAula(oAula))
                {
                    creada = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al crear el Aula: " + ex.Message);
            }
            return creada;
        }
        /// <summary>
        /// Verifica si el aula viene con el parámetro id
        /// </summary>
        /// <param name="oAula">Entidad Aula</param>
        /// <returns>True = Eliminada, False = No Eliminada</returns>
        internal bool EliminarAula(Aula oAula)
        {
            bool eliminada = false;
            try
            {
                if (oAula.Id == 0)
                {
                    throw new Exception("Error, aula sin id");
                }
                if (this.repAula.EliminarAula(oAula))
                {
                    eliminada = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return eliminada;
        }

        /// <summary>
        /// Actualiza un registro en la base de datos
        /// Envía un objeto de tipo Aula para poder realizar la actulización de los datos.
        /// </summary>
        /// <param name="oAula">Entidad Aula</param>
        /// <returns>True = Actualizada, False = No actualizada</returns>
        internal bool ActualizarAula(Aula oAula)
        {
            bool actualizada = false;
            try
            {
                if (String.IsNullOrEmpty(oAula.Descripcion) || oAula.Id == 0 || oAula.Numero == 0)
                {
                    throw new Exception("Error al actualizar el Aula campos incompletos");
                }
                if (this.repAula.ActualizarAula(oAula))
                {
                    actualizada = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al actualizar Aula: " + ex.Message);
            }
            return actualizada;
        }
    }
}
