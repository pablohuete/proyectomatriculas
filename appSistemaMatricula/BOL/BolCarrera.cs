﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sistemaMatricula.Repositorios;
using sistemaMatricula.Entidades;

namespace appSistemaMatricula.BOL
{
    public class BolCarrera
    {
        private RepCarrera repCarrera;

        public BolCarrera()
        {
            this.repCarrera = new RepCarrera();
        }
        /// <summary>
        /// Trae una lista de carreras de la base de datos
        /// </summary>
        /// <returns>Arreglo de Carreras</returns>
        internal List<Carrera> ObtenerCarreras()
        {
            List<Carrera> periodos = new List<Carrera>();
            try
            {
                periodos = this.repCarrera.ObtenerCarreras();
            }
            catch (Exception ex)
            {
                new Exception("Error al cargar los Carrera : " + ex.Message);
            }
            return periodos;
        }
        /// <summary>
        /// Valida los criterios de búsqueda para obtener una Carrera
        /// Solicitando un objeto de este tipo para determinar los criterios de búsqueda
        /// </summary>
        /// <param name="oCarrera">Entidad Carrera</param>
        /// <returns>Entidad Carrera</returns>
        internal List<Carrera> BuscarCarreras(Carrera oCarrera)
        {
            List<Carrera> carreras;
            try
            {
                if (String.IsNullOrEmpty(oCarrera.CodigoCarrera) || String.IsNullOrEmpty(oCarrera.Descripcion))
                {
                    throw new Exception("Criterio de búsqueda erróneo");
                }                
                carreras = this.repCarrera.BuscarCarreras(oCarrera);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al consultar Carrera: " + ex.Message);
            }
            return carreras;
        }
        /// <summary>
        /// Guarda una Entidad Carrera en la base de datos
        /// Valida que el objeto no traiga datos erróneos o nulos
        /// </summary>
        /// <param name="oCarrera">Entidad Carrera</param>
        /// <returns>True = Creada, False = No creada</returns>
        internal bool CrearCarrera(Carrera oCarrera)
        {
            bool creado = false;
            try
            {
                if (String.IsNullOrEmpty(oCarrera.CodigoCarrera) || String.IsNullOrEmpty(oCarrera.Descripcion))
                {
                    throw new Exception("Datos de la Carrera incompletos");
                }
                
                if (this.repCarrera.CrearCarrera(oCarrera))
                {
                    creado = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al crear el Carrera: " + ex.Message);
            }
            return creado;
        }
        /// <summary>
        /// Verifica si el carrera viene con el parámetro id
        /// </summary>
        /// <param name="oCarrera">Entidad Carrera</param>
        /// <returns>True = Eliminada, False = No Eliminada</returns>
        internal bool EliminarCarrera(Carrera oCarrera)
        {
            bool eliminada = false;
            try
            {
                if (oCarrera.Id.ToString().Equals(""))
                {
                    throw new Exception("Error, carrera sin id");
                }
                if (this.repCarrera.EliminarCarrera(oCarrera))
                {
                    eliminada = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return eliminada;
        }
        /// <summary>
        /// Actualiza un registro en la base de datos
        /// Envía un objeto de tipo Carrera para poder realizar la actulización de los datos.
        /// </summary>
        /// <param name="oCarrera">Entidad Carrera</param>
        /// <returns>True = Actualizada, False = No actualizada</returns>
        internal bool ActualizarCarrera(Carrera oCarrera)
        {
            bool actualizado = false;
            try
            {
                if (String.IsNullOrEmpty(oCarrera.CodigoCarrera) || String.IsNullOrEmpty(oCarrera.Descripcion)
                    || oCarrera.Id == 0)
                {
                    throw new Exception("Error al actualizar el Carrera campos incompletos");
                }
                if (this.repCarrera.ActualizarCarrera(oCarrera))
                {
                    actualizado = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al actualizar Carrera: " + ex.Message);
            }
            return actualizado;
        }
    }
}
