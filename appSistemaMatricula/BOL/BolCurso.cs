﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sistemaMatricula.Repositorios;
using sistemaMatricula.Entidades;

namespace appSistemaMatricula.BOL
{
    public class BolCurso
    {
        private RepCurso repCurso;

        public BolCurso()
        {
            this.repCurso = new RepCurso();
        }
        /// <summary>
        /// Trae una lista de Cursos de la base de datos
        /// </summary>
        /// <returns>Arreglo de Cursos</returns>
        internal List<Curso> CargarCursos()
        {
            List<Curso> cursos = new List<Curso>();
            try
            {
                cursos = this.repCurso.ObtenerCursos();
            }
            catch (Exception ex)
            {
                throw new Exception("Error al cargar los Cursos: " + ex.Message);
            }
            return cursos;
        }
        /// <summary>
        /// Búsqueda Filtrada de Cursos
        /// Valida los datos del Curso para hacer una consulta en la base de datos
        /// </summary>
        /// <param name="oCurso">Entidad Curso</param>
        /// <returns>Lista filtrada de Cursos</returns>
        internal List<Curso> CargarCursos(Curso oCurso)
        {
            List<Curso> cursos = new List<Curso>();
            try
            {
                if (String.IsNullOrEmpty(oCurso.CodigoCurso)
                    || String.IsNullOrEmpty(oCurso.Descripcion))                   
                {
                    throw new Exception("Datos requeridos incompletos o incorrectos");
                }
                cursos = this.repCurso.BuscarCursos(oCurso);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar: " + ex.Message);
            }
            return cursos;
        }
        /// <summary>
        /// Valida los datos para crear o actualizar un objeto Curso
        /// </summary>
        /// <param name="oCurso">Entidad Curso</param>
        /// <returns>True = Creado o actualizado, False = No creado o actualizado</returns>
        internal bool ValidarCrearActualizar(Curso oCurso)
        {            
            try
            {
                if (String.IsNullOrEmpty(oCurso.CodigoCurso)
                    || String.IsNullOrEmpty(oCurso.Descripcion))
                {
                    throw new Exception("Datos requeridos");
                }
                if (oCurso.CantidadCreditos < 0)
                {
                    throw new Exception("Los cantidad de créditos debe ser un número positivo");
                }
                if (oCurso.Id > 0)
                {
                    return this.repCurso.ActualizarCurso(oCurso);
                }
                else
                {
                    return this.repCurso.CrearCurso(oCurso);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Verifica si el curso a eliminar viene con Id
        /// </summary>
        /// <param name="oCurso">Entidad Curso</param>
        /// <returns>True = Eliminado, False = No eliminado</returns>
        internal bool EliminarCurso(Curso oCurso)
        {
            bool eliminado = false;
            try
            {
                if (oCurso.Id > 0)
                {
                    eliminado = this.repCurso.EliminarCurso(oCurso);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return eliminado;
        }
    }
}
