﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sistemaMatricula.Repositorios;
using sistemaMatricula.Entidades;

namespace appSistemaMatricula.BOL
{
    public class BolEstudiante
    {
        private RepEstudiante repEstudiante { get; set; }
        public BolEstudiante()
        {
            this.repEstudiante = new RepEstudiante();
        }
        /// <summary>
        /// Trae una lista de Estudiantes de la base de datos
        /// </summary>
        /// <returns>Arreglo de Estudiantes</returns>
        internal List<Estudiante> CargarEstudiantes()
        {
            List<Estudiante> estudiantes = new List<Estudiante>();
            try
            {
                estudiantes = this.repEstudiante.CargarEstudiantes();
            }
            catch (Exception ex)
            {
               throw new Exception("Error al cargar los Estudiantes: " + ex.Message);
            }
            return estudiantes;
        }
        /// <summary>
        /// Búsqueda Filtrada de Estudiates
        /// Valida los datos del Estudiante para hacer una consulta en la base de datos
        /// </summary>
        /// <param name="oEstudiante">Entidad Estudiante</param>
        /// <returns>Lista filtrada de Estudiantes</returns>
        internal List<Estudiante> CargarEstudiantes(Estudiante oEstudiante)
        {
            List<Estudiante> estudiantes = new List<Estudiante>();
            try
            {
                if (String.IsNullOrEmpty(oEstudiante.Cedula))                    
                {
                    throw new Exception("Datos requeridos incompletos");
                }
                estudiantes = this.repEstudiante.CargarEstudiantes(oEstudiante);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar: " + ex.Message);
            }
            return estudiantes;
        }
        /// <summary>
        /// Valida los datos para crear o actualizar un objeto Estudiante
        /// </summary>
        /// <param name="oEstudiante">Entidad Estudiante</param>
        /// <returns>True = Creado o actualizado, False = No creado o actualizado</returns>
        internal bool ValidarCrearActualizar(Estudiante oEstudiante)
        {
            try
            {
                if (String.IsNullOrEmpty(oEstudiante.Nombre)
                    || String.IsNullOrEmpty(oEstudiante.Cedula)
                    || String.IsNullOrEmpty(oEstudiante.EstaActivo.ToString())
                    || String.IsNullOrEmpty(oEstudiante.Promedio.ToString()))
                {
                    throw new Exception("Datos requeridos");
                }

                if (oEstudiante.Id > 0)
                {
                    return this.repEstudiante.ActualizarEstudiante(oEstudiante);
                }
                else
                {
                    return this.repEstudiante.CrearEstudiante(oEstudiante);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }        
        /// <summary>
        /// Verifica si el estudiante a eliminar viene con Id
        /// </summary>
        /// <param name="oEstudiante">Entidad Estudiante</param>
        /// <returns>True = Eliminado, False = No eliminado</returns>
        internal bool EliminarEstudiante(Estudiante oEstudiante)
        {
            bool eliminado = false;
            try
            {
                if (oEstudiante.Id > 0)
                {
                    eliminado = this.repEstudiante.EliminarEstudiante(oEstudiante);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return eliminado;
        }
    }
}
