﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sistemaMatricula.Repositorios;
using sistemaMatricula.Entidades;

namespace appSistemaMatricula.BOL
{
    public class BolPeriodo
    {
        private RepPeriodo repPeriodo;

        public BolPeriodo()
        {
            this.repPeriodo = new RepPeriodo();
        }
        /// <summary>
        /// Trae una lista de períodos de la base de datos
        /// </summary>
        /// <returns>Arreglo de Períodos</returns>
        internal List<Periodo> ObtenerPeriodos()
        {
            List<Periodo> periodos = new List<Periodo>();
            try
            {
                periodos = this.repPeriodo.ObtenerPeriodos();
            }
            catch (Exception ex)
            {
                new Exception("Error al cargar los Períodos : " + ex.Message);
            }
            return periodos;
        }
        /// <summary>
        /// Valida los criterios de búsqueda para obtener un Periodo
        /// Solicitando un objeto de este tipo para determinar los criterios de búsqueda
        /// </summary>
        /// <param name="oPeriodo">Entidad Periodo</param>
        /// <returns>Entidad Periodo</returns>
        internal List<Periodo> BuscarPeriodos(Periodo oPeriodo)
        {
            List<Periodo> periodos;
            try
            {
                if (String.IsNullOrEmpty(oPeriodo.Descripcion))
                {
                    throw new Exception("Criterio de búsqueda erróneo");
                }
                if (oPeriodo.FechaInicio.Date >= oPeriodo.FechaFin.Date)
                {
                    throw new Exception("Fechas No Correctas");
                }
                periodos = this.repPeriodo.BuscarPeriodos(oPeriodo);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al consultar Periodo: " + ex.Message);
            }
            return periodos;
        }
        /// <summary>
        /// Guarda una Entidad Periodo en la base de datos
        /// Valida que el objeto no traiga datos erróneos o nulos
        /// </summary>
        /// <param name="oPeriodo"></param>
        /// <returns>True = Creada, False = No creada</returns>
        internal bool CrearPeriodo(Periodo oPeriodo)
        {
            bool creado = false;
            try
            {
                if (String.IsNullOrEmpty(oPeriodo.Descripcion) || oPeriodo.FechaInicio == DateTime.MinValue 
                    || oPeriodo.FechaFin == DateTime.MaxValue)
                {
                    throw new Exception("Datos del período incompletos");
                }
                if (oPeriodo.FechaInicio.Date >= oPeriodo.FechaFin.Date)
                {
                    throw new Exception("Fechas no válidas");
                }
                if (this.repPeriodo.CrearPeriodo(oPeriodo))
                {
                    creado = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al crear el Período: " + ex.Message);
            }
            return creado;
        }
        /// <summary>
        /// Verifica si el periodo viene con el parámetro id
        /// </summary>
        /// <param name="oPeriodo">Entidad Periodo</param>
        /// <returns>True = Eliminada, False = No Eliminada</returns>
        internal bool EliminarPeriodo(Periodo oPeriodo)
        {
            bool eliminada = false;
            try
            {
                if (oPeriodo.Id.ToString().Equals(""))
                {
                    throw new Exception("Error, período sin id");
                }
                if (this.repPeriodo.EliminarPeriodo(oPeriodo))
                {
                    eliminada = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return eliminada;
        }
        /// <summary>
        /// Actualiza un registro en la base de datos
        /// Envía un objeto de tipo Período para poder realizar la actulización de los datos.
        /// </summary>
        /// <param name="oPeriodo">Entidad Periodo</param>
        /// <returns>True = Actualizado, False = No actualizado</returns>
        internal bool ActualizarPerido(Periodo oPeriodo)
        {
            bool actualizado = false;
            try
            {
                if (String.IsNullOrEmpty(oPeriodo.Descripcion) || oPeriodo.Id == 0 
                    || oPeriodo.FechaInicio == DateTime.MinValue || oPeriodo.FechaFin == DateTime.MaxValue)
                {
                    throw new Exception("Error al actualizar el Período campos incompletos");
                }
                if (this.repPeriodo.ActualizarPeriodo(oPeriodo))
                {
                    actualizado = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al actualizar Período: " + ex.Message);
            }
            return actualizado;
        }
    }
}
