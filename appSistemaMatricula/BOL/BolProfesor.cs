﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sistemaMatricula.Repositorios;
using sistemaMatricula.Entidades;

namespace appSistemaMatricula.BOL
{
    public class BolProfesor
    {
        public RepProfesor repProfesor { get; set; }

        public BolProfesor()
        {
            this.repProfesor = new RepProfesor();
        }
        /// <summary>
        /// Trae una lista de Profesor de la base de datos
        /// </summary>
        /// <returns>Arreglo de Profesores</returns>
        internal List<Profesor> CargarProfesores()
        {
            List<Profesor> profesores = new List<Profesor>();
            try
            {
                profesores = this.repProfesor.CargarProfesores();
            }
            catch (Exception ex)
            {
                throw new Exception("Error al cargar los Profesores: " + ex.Message);
            }
            return profesores;
        }
        /// <summary>
        /// Búsqueda Filtrada de Profesores
        /// Valida los datos del Profesor para hacer una consulta en la base de datos
        /// </summary>
        /// <param name="oProfesor">Entidad Profesor</param>
        /// <returns>Lista filtrada de Profesores</returns>
        internal List<Profesor> CargarProfesores(Profesor oProfesor)
        {
            List<Profesor> profesores = new List<Profesor>();
            try
            {
                if (String.IsNullOrEmpty(oProfesor.Cedula))
                {
                    throw new Exception("Datos requeridos incompletos");
                }
                profesores = this.repProfesor.CargarProfesores(oProfesor);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar: " + ex.Message);
            }
            return profesores;
        }
        /// <summary>
        /// Valida los datos para crear o actualizar un objeto Profesor
        /// </summary>
        /// <param name="oProfesor">Entidad Profesor</param>
        /// <returns>True = Creado o actualizado, False = No creado o actualizado</returns>
        internal bool ValidarCrearActualizar(Profesor oProfesor)
        {
            try
            {
                if (String.IsNullOrEmpty(oProfesor.Nombre)
                    || String.IsNullOrEmpty(oProfesor.Cedula))
                    
                {
                    throw new Exception("Datos requeridos");
                }

                if (oProfesor.Id > 0)
                {
                    return this.repProfesor.ActualizarProfesor(oProfesor);
                }
                else
                {
                    return this.repProfesor.CrearProfesor(oProfesor);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Verifica si el estudiante a eliminar viene con Id
        /// </summary>
        /// <param name="oProfesor">Entidad Profesor</param>
        /// <returns>True = Eliminado, False = No eliminado</returns>
        internal bool EliminarProfesor(Profesor oProfesor)
        {
            bool eliminado = false;
            try
            {
                if (oProfesor.Id > 0)
                {
                    eliminado = this.repProfesor.EliminarProfesor(oProfesor);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return eliminado;
        }
    }
}
