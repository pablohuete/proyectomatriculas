﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sistemaMatricula.Entidades;
using sistemaMatricula.Repositorios;

namespace appSistemaMatricula
{
    class BolUsuario
    {

        private RepUsuario udal;

        public BolUsuario()
        {
            udal = new RepUsuario();
        }
        /// <summary>
        /// Valida los datos necesarios para el logear el usuario
        /// </summary>
        /// <param name="u">Entidad Usuario</param>
        /// <returns>Entidad Usuario</returns>
        internal Usuario Login(Usuario u)
        {
            if (String.IsNullOrEmpty(u.Contrasena) || String.IsNullOrEmpty(u.NombreUsuario))
            {
                throw new Exception("Usuario y contraseña requeridos");
            }


            if (!ValidarPassword(u.Contrasena))
            {
                throw new Exception("Credenciales inválidas");
            }

            return udal.Login(u);
        }
        /// <summary>
        /// Valida si se debe filtrar el nombre de usuario o cargar todos
        /// los datos
        /// </summary>
        /// <param name="filtro">String para la búsqueda</param>
        /// <returns>Lista de usuarios</returns>
        internal List<Usuario> CargarUsuarios(string filtro = "")
        {
            try
            {
                if (!String.IsNullOrEmpty(filtro))
                {
                    return udal.CargarUsuarios(filtro);
                }
                return udal.ObtenerUsuarios();
            }
            catch (Exception ex)
            {
                throw ex;
            }         
        }
        /// <summary>
        /// Valida los datos necesarios para Crear o Actualizar un Usuario
        /// </summary>
        /// <param name="oUsuario"></param>
        /// <param name="repetirContrasena"></param>
        /// <returns></returns>
        internal bool ValidarCrearActualizar(Usuario oUsuario, string repetirContrasena)
        {
            try
            {
                if (String.IsNullOrEmpty(oUsuario.Contrasena) || String.IsNullOrEmpty(oUsuario.NombreUsuario))
                {
                    throw new Exception("Datos requeridos");
                }

                if (!ValidarPassword(oUsuario.Contrasena))
                {
                    throw new Exception("Constraseña no cumple con las normas de seguridad");
                }

                if (!oUsuario.Contrasena.Equals(repetirContrasena))
                {
                    throw new Exception("Contraseñas no coinciden");
                }
                if (oUsuario.Id > 0)
                {
                   return udal.ActualizarUsuario(oUsuario);
                }
                else
                {
                   return udal.CrearNuevo(oUsuario);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }

        //internal List<TipoUsuario> CargarTipos()
        //{
        //    return udal.CargarTiposUsuario(false);
        //}

        /// <summary>
        /// Comprueba que la contraseña contenga más de 7 caracteres
        /// </summary>
        /// <param name="pass"></param>
        /// <returns></returns>
        private bool ValidarPassword(string pass)
        {
            //TODO: todas las demás caracterisiticas de seguridad de la contraseña
            return pass.Length > 7;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="oUsuario"></param>
        /// <returns>True = Elimado, False = no eliminado</returns>
        internal bool EliminarUsuario(Usuario oUsuario)
        {
            bool eliminado = false;
            try
            {
                if (oUsuario.Id > 0)
                {
                    eliminado = udal.EliminarUsuario(oUsuario);
                }             
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return eliminado;
        }
    }
}
