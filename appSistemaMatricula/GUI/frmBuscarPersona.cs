﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using sistemaMatricula.Entidades;

namespace appSistemaMatricula.GUI
{
    public partial class frmBuscarPersona : Form
    {
        public Persona Persona { internal get; set; }
        public frmBuscarPersona(Persona pPersona)
        {
            InitializeComponent();
            this.Persona = pPersona;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(this.txtCedula.Text))
            {
                this.DialogResult = DialogResult.OK;
                this.Persona.Cedula = this.txtCedula.Text.Trim();
            }
            else
            {
                MessageBox.Show("Digite la cédula");
            }
        }
    }
}
