﻿namespace appSistemaMatricula.GUI
{
    partial class frmDefinicionGrupos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDefinicionGrupos));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gbDatos = new System.Windows.Forms.GroupBox();
            this.dtpFechaFin = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.cbCuatrimestres = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dtpFechaInicio = new System.Windows.Forms.DateTimePicker();
            this.cbAulas = new System.Windows.Forms.ComboBox();
            this.cbProfesores = new System.Windows.Forms.ComboBox();
            this.cbCursos = new System.Windows.Forms.ComboBox();
            this.cbCarreras = new System.Windows.Forms.ComboBox();
            this.cbPeriodos = new System.Windows.Forms.ComboBox();
            this.btnAsignar = new System.Windows.Forms.Button();
            this.btnFiltrar = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tsBarra = new System.Windows.Forms.ToolStrip();
            this.btnEditar = new System.Windows.Forms.ToolStripButton();
            this.btnEliminar = new System.Windows.Forms.ToolStripButton();
            this.btnResfrescar = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvGrupos = new System.Windows.Forms.DataGridView();
            this.ColumnaId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnaNumero = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnaDias = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnaHoraInicio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnaHoraFin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnaPeriodo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnaCurso = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnaCupo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnaProfesor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnaNotaFinal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnaAula = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnaActivo = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label8 = new System.Windows.Forms.Label();
            this.cbDias = new System.Windows.Forms.ComboBox();
            this.nudCupos = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.gbDatos.SuspendLayout();
            this.tsBarra.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGrupos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCupos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // gbDatos
            // 
            this.gbDatos.Controls.Add(this.numericUpDown1);
            this.gbDatos.Controls.Add(this.label10);
            this.gbDatos.Controls.Add(this.label9);
            this.gbDatos.Controls.Add(this.nudCupos);
            this.gbDatos.Controls.Add(this.cbDias);
            this.gbDatos.Controls.Add(this.label8);
            this.gbDatos.Controls.Add(this.dtpFechaFin);
            this.gbDatos.Controls.Add(this.label7);
            this.gbDatos.Controls.Add(this.cbCuatrimestres);
            this.gbDatos.Controls.Add(this.label6);
            this.gbDatos.Controls.Add(this.dtpFechaInicio);
            this.gbDatos.Controls.Add(this.cbAulas);
            this.gbDatos.Controls.Add(this.cbProfesores);
            this.gbDatos.Controls.Add(this.cbCursos);
            this.gbDatos.Controls.Add(this.cbCarreras);
            this.gbDatos.Controls.Add(this.cbPeriodos);
            this.gbDatos.Controls.Add(this.btnAsignar);
            this.gbDatos.Controls.Add(this.btnFiltrar);
            this.gbDatos.Controls.Add(this.label5);
            this.gbDatos.Controls.Add(this.label4);
            this.gbDatos.Controls.Add(this.label3);
            this.gbDatos.Controls.Add(this.label2);
            this.gbDatos.Controls.Add(this.label1);
            this.gbDatos.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbDatos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbDatos.Location = new System.Drawing.Point(0, 0);
            this.gbDatos.Name = "gbDatos";
            this.gbDatos.Size = new System.Drawing.Size(844, 244);
            this.gbDatos.TabIndex = 0;
            this.gbDatos.TabStop = false;
            this.gbDatos.Text = "Grupos";
            // 
            // dtpFechaFin
            // 
            this.dtpFechaFin.Checked = false;
            this.dtpFechaFin.CustomFormat = "dd/MM/yy hh:mm tt";
            this.dtpFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFechaFin.Location = new System.Drawing.Point(145, 118);
            this.dtpFechaFin.Name = "dtpFechaFin";
            this.dtpFechaFin.ShowCheckBox = true;
            this.dtpFechaFin.Size = new System.Drawing.Size(172, 21);
            this.dtpFechaFin.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 118);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 16);
            this.label7.TabIndex = 15;
            this.label7.Text = "Fecha Final:";
            // 
            // cbCuatrimestres
            // 
            this.cbCuatrimestres.FormattingEnabled = true;
            this.cbCuatrimestres.Items.AddRange(new object[] {
            "I Cuatrimestre",
            "II Cuatrimestre",
            "III Cuatrimestre"});
            this.cbCuatrimestres.Location = new System.Drawing.Point(444, 203);
            this.cbCuatrimestres.Name = "cbCuatrimestres";
            this.cbCuatrimestres.Size = new System.Drawing.Size(110, 23);
            this.cbCuatrimestres.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 68);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 16);
            this.label6.TabIndex = 13;
            this.label6.Text = "Fecha Inicio:";
            // 
            // dtpFechaInicio
            // 
            this.dtpFechaInicio.Checked = false;
            this.dtpFechaInicio.CustomFormat = "dd/MM/yy hh:mm tt";
            this.dtpFechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFechaInicio.Location = new System.Drawing.Point(145, 68);
            this.dtpFechaInicio.Name = "dtpFechaInicio";
            this.dtpFechaInicio.ShowCheckBox = true;
            this.dtpFechaInicio.Size = new System.Drawing.Size(172, 21);
            this.dtpFechaInicio.TabIndex = 12;
            // 
            // cbAulas
            // 
            this.cbAulas.FormattingEnabled = true;
            this.cbAulas.Location = new System.Drawing.Point(606, 131);
            this.cbAulas.Name = "cbAulas";
            this.cbAulas.Size = new System.Drawing.Size(208, 23);
            this.cbAulas.TabIndex = 11;
            // 
            // cbProfesores
            // 
            this.cbProfesores.FormattingEnabled = true;
            this.cbProfesores.Location = new System.Drawing.Point(606, 25);
            this.cbProfesores.Name = "cbProfesores";
            this.cbProfesores.Size = new System.Drawing.Size(208, 23);
            this.cbProfesores.TabIndex = 9;
            // 
            // cbCursos
            // 
            this.cbCursos.FormattingEnabled = true;
            this.cbCursos.Location = new System.Drawing.Point(606, 69);
            this.cbCursos.Name = "cbCursos";
            this.cbCursos.Size = new System.Drawing.Size(208, 23);
            this.cbCursos.TabIndex = 10;
            // 
            // cbCarreras
            // 
            this.cbCarreras.FormattingEnabled = true;
            this.cbCarreras.Location = new System.Drawing.Point(145, 182);
            this.cbCarreras.Name = "cbCarreras";
            this.cbCarreras.Size = new System.Drawing.Size(172, 23);
            this.cbCarreras.TabIndex = 9;
            // 
            // cbPeriodos
            // 
            this.cbPeriodos.FormattingEnabled = true;
            this.cbPeriodos.Location = new System.Drawing.Point(145, 26);
            this.cbPeriodos.Name = "cbPeriodos";
            this.cbPeriodos.Size = new System.Drawing.Size(172, 23);
            this.cbPeriodos.TabIndex = 8;
            // 
            // btnAsignar
            // 
            this.btnAsignar.Location = new System.Drawing.Point(708, 203);
            this.btnAsignar.Name = "btnAsignar";
            this.btnAsignar.Size = new System.Drawing.Size(75, 23);
            this.btnAsignar.TabIndex = 7;
            this.btnAsignar.Text = "Asignar";
            this.btnAsignar.UseVisualStyleBackColor = true;
            this.btnAsignar.Click += new System.EventHandler(this.btnAsignar_Click);
            // 
            // btnFiltrar
            // 
            this.btnFiltrar.Location = new System.Drawing.Point(578, 203);
            this.btnFiltrar.Name = "btnFiltrar";
            this.btnFiltrar.Size = new System.Drawing.Size(75, 23);
            this.btnFiltrar.TabIndex = 6;
            this.btnFiltrar.Text = "Filtrar";
            this.btnFiltrar.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(529, 134);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 16);
            this.label5.TabIndex = 5;
            this.label5.Text = "Aula:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(529, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 16);
            this.label4.TabIndex = 4;
            this.label4.Text = "Profesor:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(529, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = "Curso:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 182);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Carreras:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Período:";
            // 
            // tsBarra
            // 
            this.tsBarra.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.tsBarra.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnEditar,
            this.btnEliminar,
            this.btnResfrescar});
            this.tsBarra.Location = new System.Drawing.Point(0, 244);
            this.tsBarra.Name = "tsBarra";
            this.tsBarra.Size = new System.Drawing.Size(844, 38);
            this.tsBarra.TabIndex = 7;
            // 
            // btnEditar
            // 
            this.btnEditar.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnEditar.Image = ((System.Drawing.Image)(resources.GetObject("btnEditar.Image")));
            this.btnEditar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(43, 35);
            this.btnEditar.Text = "Editar";
            this.btnEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // btnEliminar
            // 
            this.btnEliminar.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnEliminar.Image = ((System.Drawing.Image)(resources.GetObject("btnEliminar.Image")));
            this.btnEliminar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(55, 35);
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnEliminar.ToolTipText = "Eliminar";
            // 
            // btnResfrescar
            // 
            this.btnResfrescar.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnResfrescar.Image = ((System.Drawing.Image)(resources.GetObject("btnResfrescar.Image")));
            this.btnResfrescar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnResfrescar.Name = "btnResfrescar";
            this.btnResfrescar.Size = new System.Drawing.Size(65, 35);
            this.btnResfrescar.Text = "Refrescar";
            this.btnResfrescar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvGrupos);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 282);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(844, 176);
            this.panel1.TabIndex = 8;
            // 
            // dgvGrupos
            // 
            this.dgvGrupos.AllowUserToAddRows = false;
            this.dgvGrupos.AllowUserToDeleteRows = false;
            this.dgvGrupos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnaId,
            this.ColumnaNumero,
            this.ColumnaDias,
            this.ColumnaHoraInicio,
            this.ColumnaHoraFin,
            this.ColumnaPeriodo,
            this.ColumnaCurso,
            this.ColumnaCupo,
            this.ColumnaProfesor,
            this.ColumnaNotaFinal,
            this.ColumnaAula,
            this.ColumnaActivo});
            this.dgvGrupos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvGrupos.Location = new System.Drawing.Point(0, 0);
            this.dgvGrupos.MultiSelect = false;
            this.dgvGrupos.Name = "dgvGrupos";
            this.dgvGrupos.ReadOnly = true;
            this.dgvGrupos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGrupos.Size = new System.Drawing.Size(844, 176);
            this.dgvGrupos.TabIndex = 8;
            // 
            // ColumnaId
            // 
            this.ColumnaId.DataPropertyName = "Id";
            this.ColumnaId.HeaderText = "Id";
            this.ColumnaId.Name = "ColumnaId";
            this.ColumnaId.ReadOnly = true;
            this.ColumnaId.Visible = false;
            // 
            // ColumnaNumero
            // 
            this.ColumnaNumero.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.ColumnaNumero.DataPropertyName = "Numero";
            this.ColumnaNumero.HeaderText = "Número";
            this.ColumnaNumero.Name = "ColumnaNumero";
            this.ColumnaNumero.ReadOnly = true;
            this.ColumnaNumero.Width = 69;
            // 
            // ColumnaDias
            // 
            this.ColumnaDias.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.ColumnaDias.DataPropertyName = "DiasSemana";
            this.ColumnaDias.HeaderText = "Día/s";
            this.ColumnaDias.Name = "ColumnaDias";
            this.ColumnaDias.ReadOnly = true;
            this.ColumnaDias.Width = 60;
            // 
            // ColumnaHoraInicio
            // 
            this.ColumnaHoraInicio.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.ColumnaHoraInicio.DataPropertyName = "HoraInicio";
            dataGridViewCellStyle7.Format = "t";
            dataGridViewCellStyle7.NullValue = null;
            this.ColumnaHoraInicio.DefaultCellStyle = dataGridViewCellStyle7;
            this.ColumnaHoraInicio.HeaderText = "Hora Inicio";
            this.ColumnaHoraInicio.Name = "ColumnaHoraInicio";
            this.ColumnaHoraInicio.ReadOnly = true;
            this.ColumnaHoraInicio.Width = 83;
            // 
            // ColumnaHoraFin
            // 
            this.ColumnaHoraFin.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.ColumnaHoraFin.DataPropertyName = "HoraFin";
            dataGridViewCellStyle8.Format = "t";
            dataGridViewCellStyle8.NullValue = null;
            this.ColumnaHoraFin.DefaultCellStyle = dataGridViewCellStyle8;
            this.ColumnaHoraFin.HeaderText = "Hora Final";
            this.ColumnaHoraFin.Name = "ColumnaHoraFin";
            this.ColumnaHoraFin.ReadOnly = true;
            this.ColumnaHoraFin.Width = 80;
            // 
            // ColumnaPeriodo
            // 
            this.ColumnaPeriodo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.ColumnaPeriodo.DataPropertyName = "Periodo";
            dataGridViewCellStyle9.NullValue = "No Asignado";
            this.ColumnaPeriodo.DefaultCellStyle = dataGridViewCellStyle9;
            this.ColumnaPeriodo.HeaderText = "Período";
            this.ColumnaPeriodo.Name = "ColumnaPeriodo";
            this.ColumnaPeriodo.ReadOnly = true;
            this.ColumnaPeriodo.Width = 70;
            // 
            // ColumnaCurso
            // 
            this.ColumnaCurso.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.ColumnaCurso.DataPropertyName = "Curso";
            dataGridViewCellStyle10.NullValue = "No asignado";
            this.ColumnaCurso.DefaultCellStyle = dataGridViewCellStyle10;
            this.ColumnaCurso.HeaderText = "Curso";
            this.ColumnaCurso.Name = "ColumnaCurso";
            this.ColumnaCurso.ReadOnly = true;
            this.ColumnaCurso.Width = 59;
            // 
            // ColumnaCupo
            // 
            this.ColumnaCupo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.ColumnaCupo.DataPropertyName = "Cupos";
            this.ColumnaCupo.HeaderText = "Cupos";
            this.ColumnaCupo.Name = "ColumnaCupo";
            this.ColumnaCupo.ReadOnly = true;
            this.ColumnaCupo.Width = 62;
            // 
            // ColumnaProfesor
            // 
            this.ColumnaProfesor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.ColumnaProfesor.DataPropertyName = "Profesor";
            dataGridViewCellStyle11.NullValue = "No Asignado";
            this.ColumnaProfesor.DefaultCellStyle = dataGridViewCellStyle11;
            this.ColumnaProfesor.HeaderText = "Profesor";
            this.ColumnaProfesor.Name = "ColumnaProfesor";
            this.ColumnaProfesor.ReadOnly = true;
            this.ColumnaProfesor.Width = 71;
            // 
            // ColumnaNotaFinal
            // 
            this.ColumnaNotaFinal.DataPropertyName = "NotaFinal";
            this.ColumnaNotaFinal.HeaderText = "Nota Final";
            this.ColumnaNotaFinal.Name = "ColumnaNotaFinal";
            this.ColumnaNotaFinal.ReadOnly = true;
            // 
            // ColumnaAula
            // 
            this.ColumnaAula.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.ColumnaAula.DataPropertyName = "Aula";
            dataGridViewCellStyle12.NullValue = "No Asignada";
            this.ColumnaAula.DefaultCellStyle = dataGridViewCellStyle12;
            this.ColumnaAula.HeaderText = "Aula";
            this.ColumnaAula.Name = "ColumnaAula";
            this.ColumnaAula.ReadOnly = true;
            this.ColumnaAula.Width = 53;
            // 
            // ColumnaActivo
            // 
            this.ColumnaActivo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.ColumnaActivo.DataPropertyName = "EstaActivo";
            this.ColumnaActivo.HeaderText = "¿Está Activo?";
            this.ColumnaActivo.Name = "ColumnaActivo";
            this.ColumnaActivo.ReadOnly = true;
            this.ColumnaActivo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnaActivo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ColumnaActivo.Width = 98;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 148);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 16);
            this.label8.TabIndex = 17;
            this.label8.Text = "Día /s:";
            // 
            // cbDias
            // 
            this.cbDias.FormattingEnabled = true;
            this.cbDias.Items.AddRange(new object[] {
            "Lunes",
            "Martes",
            "Miércoles",
            "Jueves",
            "Viernes",
            "Sábado",
            "Domingo"});
            this.cbDias.Location = new System.Drawing.Point(145, 145);
            this.cbDias.Name = "cbDias";
            this.cbDias.Size = new System.Drawing.Size(172, 23);
            this.cbDias.TabIndex = 18;
            // 
            // nudCupos
            // 
            this.nudCupos.Location = new System.Drawing.Point(145, 211);
            this.nudCupos.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.nudCupos.Name = "nudCupos";
            this.nudCupos.Size = new System.Drawing.Size(120, 21);
            this.nudCupos.TabIndex = 19;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 213);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 16);
            this.label9.TabIndex = 20;
            this.label9.Text = "Cupos:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(359, 30);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 16);
            this.label10.TabIndex = 21;
            this.label10.Text = "Nota Final";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.DecimalPlaces = 1;
            this.numericUpDown1.Location = new System.Drawing.Point(444, 28);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(70, 21);
            this.numericUpDown1.TabIndex = 22;
            // 
            // frmDefinicionGrupos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 458);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tsBarra);
            this.Controls.Add(this.gbDatos);
            this.MinimumSize = new System.Drawing.Size(700, 450);
            this.Name = "frmDefinicionGrupos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Definición de Grupos";
            this.Load += new System.EventHandler(this.frmDefinicionGrupos_Load);
            this.gbDatos.ResumeLayout(false);
            this.gbDatos.PerformLayout();
            this.tsBarra.ResumeLayout(false);
            this.tsBarra.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGrupos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCupos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbDatos;
        private System.Windows.Forms.ComboBox cbAulas;
        private System.Windows.Forms.ComboBox cbProfesores;
        private System.Windows.Forms.ComboBox cbCursos;
        private System.Windows.Forms.ComboBox cbCarreras;
        private System.Windows.Forms.ComboBox cbPeriodos;
        private System.Windows.Forms.Button btnAsignar;
        private System.Windows.Forms.Button btnFiltrar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStrip tsBarra;
        private System.Windows.Forms.ToolStripButton btnEditar;
        private System.Windows.Forms.ToolStripButton btnEliminar;
        private System.Windows.Forms.ToolStripButton btnResfrescar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgvGrupos;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnaId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnaNumero;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnaDias;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnaHoraInicio;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnaHoraFin;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnaPeriodo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnaCurso;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnaCupo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnaProfesor;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnaNotaFinal;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnaAula;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColumnaActivo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dtpFechaInicio;
        private System.Windows.Forms.ComboBox cbCuatrimestres;
        private System.Windows.Forms.DateTimePicker dtpFechaFin;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown nudCupos;
        private System.Windows.Forms.ComboBox cbDias;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label10;
    }
}