﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using appSistemaMatricula.BOL;
using sistemaMatricula.Entidades;

namespace appSistemaMatricula.GUI
{
    public partial class frmDefinicionGrupos : Form
    {
        private BolPeriodo bolPeriodo;
        private BolCarrera bolCarrera;
        private BolCurso bolCurso;
        private BolProfesor bolProfesor;
        private BolAula bolAula;
        private BolGrupo bolGrupo;
        private List<Periodo> periodos;
        private List<Carrera> carreras;
        private List<Curso> cursos;
        private List<Profesor> profesores;
        private List<Aula> aulas;
        private List<Grupo> grupos;
        public frmDefinicionGrupos()
        {
            InitializeComponent();
            this.bolPeriodo = new BolPeriodo();
            this.bolCarrera = new BolCarrera();
            this.bolCurso = new BolCurso();
            this.bolProfesor = new BolProfesor();
            this.bolAula = new BolAula();
            this.bolGrupo = new BolGrupo();
        }

        private void frmDefinicionGrupos_Load(object sender, EventArgs e)
        {
            try
            {
                /* 
                 * carga las listas
                 */
                this.CargarPeriodos();
                this.CargarCarreras();
                this.CargarCursos();
                this.CargarProfesores();
                this.CargarAulas();

                /*
                 * Carga los combos                 
                 */
                this.cbPeriodos.DataSource = this.periodos;
                this.cbCarreras.DataSource = this.carreras;
                this.cbCursos.DataSource = this.cursos;
                this.cbProfesores.DataSource = this.profesores;
                this.cbAulas.DataSource = this.aulas;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// Carga los periodos activos en a lista
        /// </summary>
        private void CargarPeriodos(bool filtrada = false)
        {
            try
            {
                
                if (!filtrada)
                {
                    this.periodos = this.bolPeriodo.ObtenerPeriodos();
                }
                else
                {
                    Periodo oPeriodo = new Periodo();
                    if (this.cbCuatrimestres.SelectedIndex != -1)
                    {
                        oPeriodo.Descripcion = cbCuatrimestres.SelectedItem.ToString();
                    }
                    else
                    {
                        oPeriodo.Descripcion = "Cuatrimestre";
                    }
                    if (this.dtpFechaInicio.Checked)
                    {
                        oPeriodo.FechaInicio = this.dtpFechaInicio.Value.Date;
                    }
                    if (this.dtpFechaFin.Checked)
                    {
                        oPeriodo.FechaFin = this.dtpFechaFin.Value.Date;
                    }
                    oPeriodo.Activo = true;

                    this.periodos = this.bolPeriodo.BuscarPeriodos(oPeriodo);
                }                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Carga los carreras en la lista
        /// </summary>
        private void CargarCarreras()
        {
            try
            {
                this.carreras = this.bolCarrera.ObtenerCarreras();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Carga los cursos en la lista
        /// </summary>
        private void CargarCursos()
        {
            try
            {
                this.cursos = this.bolCurso.CargarCursos();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Carga los profesores en la lista
        /// </summary>
        private void CargarProfesores()
        {
            try
            {
                this.profesores = this.bolProfesor.CargarProfesores();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Carga las aulas en la lista
        /// </summary>
        private void CargarAulas()
        {
            try
            {
                this.aulas = this.bolAula.ObtenerAulas();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnAsignar_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {            
                throw ex;
            }
        }
    }
}
