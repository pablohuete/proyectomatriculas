﻿using sistemaMatricula.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace appSistemaMatricula.GUI
{
    public partial class frmLogin : Form
    {
        private BolUsuario bolUsuario;
        public frmLogin()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Limpia los campos de entrada de texto
        /// </summary>
        private void LimpiarCampos()
        {
            this.txtUsuario.Text = "";
            this.txtContrasena.Text = "";
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                Usuario eUsuario = new Usuario {
                    NombreUsuario = this.txtUsuario.Text,
                    Contrasena = this.txtContrasena.Text
                };
                eUsuario = bolUsuario.Login(eUsuario);
                
                if (eUsuario.Id > 0)
                {
                    this.Hide();
                    new frmMenu(eUsuario).ShowDialog();
                    this.LimpiarCampos();
                    this.Show();
                }
                else
                {
                    MessageBox.Show("Fallo al iniciar sesión");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            this.bolUsuario = new BolUsuario();
        }
    }
}
