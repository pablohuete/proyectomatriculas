﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using appSistemaMatricula.BOL;
using sistemaMatricula.Entidades;

namespace appSistemaMatricula.GUI
{
    public partial class frmMantAulas : Form
    {
        private BolAula bolAula;
        public frmMantAulas()
        {
            this.bolAula = new BolAula();
            InitializeComponent();
        }

        private void frmMantAulas_Load(object sender, EventArgs e)
        {
            this.CargarAulas();
        }

        /// <summary>
        /// Recarga el DataGridView que muestra los datos
        /// </summary>
        private void CargarAulas()
        {
            try
            {
                //this.dgvAulas.DataSource = null;
                this.dgvAulas.DataSource = bolAula.ObtenerAulas();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            try
            {
                frmNuevaAula frmNuevaAula = new frmNuevaAula();
                frmNuevaAula.Text = "Crear Nueva Aula";
                if (frmNuevaAula.ShowDialog() == DialogResult.OK)
                {
                    if (this.bolAula.CrearAula(frmNuevaAula.oAula))
                    {
                        MessageBox.Show("Datos agregados correctamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.CargarAulas();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); ;
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvAulas.SelectedCells.Count > 0)
                {
                    frmNuevaAula frmNuevaAula = new frmNuevaAula();
                    frmNuevaAula.Text = "Editar Aula";
                    Aula oAula = new Aula();
                    DataGridViewSelectedCellCollection celdas = this.dgvAulas.SelectedCells;
                    oAula.Id = Int32.Parse(celdas[0].Value.ToString());
                    oAula.Descripcion = celdas[1].Value.ToString();
                    oAula.Numero = Int32.Parse(celdas[2].Value.ToString());

                    frmNuevaAula.oAula = oAula;

                    if (frmNuevaAula.ShowDialog() == DialogResult.OK)
                    {
                        if (this.bolAula.ActualizarAula(frmNuevaAula.oAula))
                        {
                            MessageBox.Show("Datos guardados correctamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.CargarAulas();
                        }                                                                        
                    }
                    else
                    {
                        MessageBox.Show("Operacion Cancelada", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Ningún registro seleccionado", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvAulas.SelectedCells.Count > 0)
                {
                    if (DialogResult.OK == MessageBox.Show("¿Realmente desea eliminar el registro?",
                    "", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning))
                    {
                        DataGridViewSelectedCellCollection celdas = this.dgvAulas.SelectedCells;
                        Aula oAula = new Aula();
                        oAula.Id = Int32.Parse(celdas[0].Value.ToString());
                        if (this.bolAula.EliminarAula(oAula))
                        {
                            MessageBox.Show("Registro eliminado", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.CargarAulas();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Ningún registro seleccionado", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void btnResfrescar_Click(object sender, EventArgs e)
        {
            this.CargarAulas();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvAulas.Rows.Count > 0)
                {
                    frmNuevaAula frmNuevaAula = new frmNuevaAula();
                    frmNuevaAula.Text = "Buscar Aula";
                    if (frmNuevaAula.ShowDialog() == DialogResult.OK)
                    {
                        //this.dgvAulas.DataSource = null;
                        this.dgvAulas.DataSource = this.bolAula.BuscarAulas(frmNuevaAula.oAula);
                    }
                }
                else
                {
                    MessageBox.Show("Sin registros para buscar", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
