﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using appSistemaMatricula.BOL;
using sistemaMatricula.Entidades;

namespace appSistemaMatricula.GUI
{
    public partial class frmMantCarreras : Form
    {
        private BolCarrera bolCarrera;
        public frmMantCarreras()
        {
            this.bolCarrera = new BolCarrera();
            InitializeComponent();
        }

        private void frmMantCarreras_Load(object sender, EventArgs e)
        {
            this.CargarCarreras();
        }
        private void CargarCarreras()
        {
            try
            {
                this.dgvCarreras.DataSource = this.bolCarrera.ObtenerCarreras();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            try
            {
                frmNuevaCarrera frmNuevaCarrera = new frmNuevaCarrera();
                frmNuevaCarrera.Text = "Crear Nueva Carrera";
                if (frmNuevaCarrera.ShowDialog() == DialogResult.OK)
                {
                    if (this.bolCarrera.CrearCarrera(frmNuevaCarrera.oCarrera))
                    {
                        MessageBox.Show("Datos agregados correctamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.CargarCarreras();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvCarreras.SelectedCells.Count > 0)
                {
                    frmNuevaCarrera frmNuevaCarrera = new frmNuevaCarrera();
                    frmNuevaCarrera.Text = "Editar Carrera";
                    Carrera oCarrera = new Carrera();
                    DataGridViewSelectedCellCollection celdas = this.dgvCarreras.SelectedCells;
                    oCarrera.Id = Int32.Parse(celdas[0].Value.ToString());
                    oCarrera.CodigoCarrera = celdas[1].Value.ToString();
                    oCarrera.Descripcion = celdas[2].Value.ToString();
                    
                    frmNuevaCarrera.oCarrera = oCarrera;
                    frmNuevaCarrera.esEdicion = true;

                    if (frmNuevaCarrera.ShowDialog() == DialogResult.OK)
                    {
                        if (this.bolCarrera.ActualizarCarrera(frmNuevaCarrera.oCarrera))
                        {
                            MessageBox.Show("Datos guardados correctamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.CargarCarreras();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Operacion Cancelada", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Ningún registro seleccionado", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvCarreras.SelectedCells.Count > 0)
                {
                    if (DialogResult.OK == MessageBox.Show("¿Realmente desea eliminar el registro?",
                    "", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning))
                    {
                        DataGridViewSelectedCellCollection celdas = this.dgvCarreras.SelectedCells;
                        Carrera oCarrera = new Carrera();
                        oCarrera.Id = Int32.Parse(celdas[0].Value.ToString());
                        if (this.bolCarrera.EliminarCarrera(oCarrera))
                        {
                            MessageBox.Show("Registro eliminado", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.CargarCarreras();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Ningún registro seleccionado", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvCarreras.Rows.Count > 0)
                {
                    frmNuevaCarrera frmNuevaCarrera = new frmNuevaCarrera();
                    frmNuevaCarrera.Text = "Buscar Carrera";
                    if (frmNuevaCarrera.ShowDialog() == DialogResult.OK)
                    {
                        this.dgvCarreras.DataSource = this.bolCarrera.BuscarCarreras(frmNuevaCarrera.oCarrera);
                    }
                }
                else
                {
                    MessageBox.Show("Sin registros para buscar", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnResfrescar_Click(object sender, EventArgs e)
        {
            this.CargarCarreras();
        }
    }
}
