﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using appSistemaMatricula.BOL;
using sistemaMatricula.Entidades;

namespace appSistemaMatricula.GUI
{
    public partial class frmMantCursos : Form
    {
        private BolCurso bolCurso;
        public frmMantCursos()
        {
            this.bolCurso = new BolCurso();
            InitializeComponent();
        }

        private void frmMantCursos_Load(object sender, EventArgs e)
        {
            this.CargarCursos();
        }
        /// <summary>
        /// Recarga el DataGridView que muestra los datos
        /// </summary>
        private void CargarCursos()
        {
            try
            {
                //this.dgvAulas.DataSource = null;
                this.dgvCursos.DataSource = this.bolCurso.CargarCursos();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            try
            {
                frmNuevoCurso frmNuevoCurso = new frmNuevoCurso();
                frmNuevoCurso.Text = "Crear Nuevo Curso";
                if (frmNuevoCurso.ShowDialog() == DialogResult.OK)
                {
                    if (this.bolCurso.ValidarCrearActualizar(frmNuevoCurso.oCurso))
                    {
                        MessageBox.Show("Datos agregados correctamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.CargarCursos();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); ;
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvCursos.SelectedCells.Count > 0)
                {
                    frmNuevoCurso frmNuevoCurso = new frmNuevoCurso();
                    frmNuevoCurso.Text = "Editar Estudiante";
                    Curso oCurso = new Curso();
                    
                    DataGridViewCellCollection celdas = this.dgvCursos.CurrentRow.Cells;

                    oCurso.Id = Int32.Parse(celdas["ColumnaId"].Value.ToString());
                    oCurso.CodigoCurso = celdas["ColumnaCodigo"].Value.ToString();
                    oCurso.Descripcion = celdas["ColumnaDescripcion"].Value.ToString();
                    oCurso.CantidadCreditos = Int32.Parse(celdas["ColumnaCreditos"].Value.ToString());   

                    frmNuevoCurso.oCurso = oCurso;
                    frmNuevoCurso.esEdicion = true;
                    
                    if (frmNuevoCurso.ShowDialog() == DialogResult.OK)
                    {
                        if (this.bolCurso.ValidarCrearActualizar(frmNuevoCurso.oCurso))
                        {
                            MessageBox.Show("Datos guardados correctamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.CargarCursos();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Operacion Cancelada", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Ningún registro seleccionado", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvCursos.SelectedCells.Count > 0)
                {
                    if (DialogResult.OK == MessageBox.Show("¿Realmente desea eliminar el registro?",
                    "", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning))
                    {
                        DataGridViewCellCollection celdas = this.dgvCursos.CurrentRow.Cells;
                        Curso oAula = new Curso();
                        oAula.Id = Int32.Parse(celdas["ColumnaId"].Value.ToString());
                        if (this.bolCurso.EliminarCurso(oAula))
                        {
                            MessageBox.Show("Registro eliminado", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.CargarCursos();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Ningún registro seleccionado", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvCursos.Rows.Count > 0)
                {
                    frmNuevoCurso frmNuevoCurso = new frmNuevoCurso();
                    frmNuevoCurso.Text = "Buscar Curso";
                    if (frmNuevoCurso.ShowDialog() == DialogResult.OK)
                    {                       
                        this.dgvCursos.DataSource = this.bolCurso.CargarCursos(frmNuevoCurso.oCurso);
                    }
                }
                else
                {
                    MessageBox.Show("Sin registros para buscar", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnResfrescar_Click(object sender, EventArgs e)
        {
            this.CargarCursos();
        }
    }
}
