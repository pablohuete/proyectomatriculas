﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using appSistemaMatricula.BOL;
using sistemaMatricula.Entidades;

namespace appSistemaMatricula.GUI
{
    public partial class frmMantEstudiantes : Form
    {
        private BolEstudiante bolEstudiante;
        public frmMantEstudiantes()
        {
            this.bolEstudiante = new BolEstudiante();
            InitializeComponent();
        }

        private void frmMantEstudiantes_Load(object sender, EventArgs e)
        {
            this.CargarEstudiantes();
        }
        /// <summary>
        /// Recarga el DataGridView que muestra los datos
        /// </summary>
        private void CargarEstudiantes()
        {
            try
            {
                this.dgvEstudiantes.DataSource = this.bolEstudiante.CargarEstudiantes();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            try
            {
                frmNuevoEstudiante frmNuevoEstudiante = new frmNuevoEstudiante();
                frmNuevoEstudiante.Text = "Crear Nuevo Estudiante";
                if (frmNuevoEstudiante.ShowDialog() == DialogResult.OK)
                {
                    if (this.bolEstudiante.ValidarCrearActualizar(frmNuevoEstudiante.oEstudiante))
                    {
                        MessageBox.Show("Datos agregados correctamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.CargarEstudiantes();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvEstudiantes.SelectedCells.Count > 0)
                {
                    frmNuevoEstudiante frmNuevoEstudiante = new frmNuevoEstudiante();
                    frmNuevoEstudiante.Text = "Editar Estudiante";
                    Estudiante oEstudiante = new Estudiante();
                    Carrera oCarrera = new Carrera();
                    Usuario oUsuario = new Usuario();                    
                    DataGridViewCellCollection celdas = this.dgvEstudiantes.CurrentRow.Cells;

                    oEstudiante.Id = Int32.Parse(celdas["ColumnaId"].Value.ToString());
                    oEstudiante.Nombre = celdas["ColumnaNombre"].Value.ToString();
                    oEstudiante.Cedula = celdas["ColumnaCedula"].Value.ToString();
                    oEstudiante.Email = celdas["ColumnaEmail"].Value.ToString();
                    oCarrera = (Carrera)celdas["ColumnaCarrera"].Value;
                    oUsuario = (Usuario)celdas["ColumnaUsuario"].Value;
                    oEstudiante.EstaActivo = Boolean.Parse(celdas["ColumnaActivo"].Value.ToString());
                    oEstudiante.Promedio = Double.Parse(celdas["ColumnaPromedio"].Value.ToString());
                    oEstudiante.Carrera = oCarrera;
                    oEstudiante.Usuario = oUsuario;

                    frmNuevoEstudiante.oEstudiante = oEstudiante;
                    frmNuevoEstudiante.esEdicion = true;

                    if (frmNuevoEstudiante.ShowDialog() == DialogResult.OK)
                    {
                        if (this.bolEstudiante.ValidarCrearActualizar(frmNuevoEstudiante.oEstudiante))
                        {
                            MessageBox.Show("Datos guardados correctamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.CargarEstudiantes();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Operacion Cancelada", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Ningún registro seleccionado", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvEstudiantes.SelectedCells.Count > 0)
                {
                    if (DialogResult.OK == MessageBox.Show("¿Realmente desea eliminar el registro?",
                    "", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning))
                    {
                        DataGridViewCellCollection celdas = this.dgvEstudiantes.CurrentRow.Cells;
                        Estudiante oEstudiante = new Estudiante();
                        oEstudiante.Id = Int32.Parse(celdas["ColumnaId"].Value.ToString());
                        if (this.bolEstudiante.EliminarEstudiante(oEstudiante))
                        {
                            MessageBox.Show("Registro eliminado", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.CargarEstudiantes();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Ningún registro seleccionado", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvEstudiantes.Rows.Count > 0)
                {                     
                    Estudiante oEstudiante = new Estudiante();
                    frmBuscarPersona frmBuscarPersona = new frmBuscarPersona(oEstudiante);
                    frmBuscarPersona.Text = "Buscar Estudiante";
                    if (frmBuscarPersona.ShowDialog() == DialogResult.OK)
                    {                        
                        this.dgvEstudiantes.DataSource = this.bolEstudiante.CargarEstudiantes(oEstudiante);
                    }
                }
                else
                {
                    MessageBox.Show("Sin registros para buscar", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnResfrescar_Click(object sender, EventArgs e)
        {
            this.CargarEstudiantes();
        }
    }
}
