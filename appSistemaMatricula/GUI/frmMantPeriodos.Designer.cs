﻿namespace appSistemaMatricula.GUI
{
    partial class frmMantPeriodos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMantPeriodos));
            this.dgvPeriodos = new System.Windows.Forms.DataGridView();
            this.Columna_Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnaDescripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnaFechaInicio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnaFechaFin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnaActivo = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tsBarra = new System.Windows.Forms.ToolStrip();
            this.btnNuevo = new System.Windows.Forms.ToolStripButton();
            this.btnEditar = new System.Windows.Forms.ToolStripButton();
            this.btnEliminar = new System.Windows.Forms.ToolStripButton();
            this.btnBuscar = new System.Windows.Forms.ToolStripButton();
            this.btnResfrescar = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPeriodos)).BeginInit();
            this.tsBarra.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvPeriodos
            // 
            this.dgvPeriodos.AllowUserToAddRows = false;
            this.dgvPeriodos.AllowUserToDeleteRows = false;
            this.dgvPeriodos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Columna_Id,
            this.ColumnaDescripcion,
            this.ColumnaFechaInicio,
            this.ColumnaFechaFin,
            this.ColumnaActivo});
            this.dgvPeriodos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPeriodos.Location = new System.Drawing.Point(0, 0);
            this.dgvPeriodos.MultiSelect = false;
            this.dgvPeriodos.Name = "dgvPeriodos";
            this.dgvPeriodos.ReadOnly = true;
            this.dgvPeriodos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPeriodos.Size = new System.Drawing.Size(684, 373);
            this.dgvPeriodos.TabIndex = 6;
            // 
            // Columna_Id
            // 
            this.Columna_Id.DataPropertyName = "Id";
            this.Columna_Id.HeaderText = "Id";
            this.Columna_Id.Name = "Columna_Id";
            this.Columna_Id.ReadOnly = true;
            this.Columna_Id.Visible = false;
            // 
            // ColumnaDescripcion
            // 
            this.ColumnaDescripcion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.ColumnaDescripcion.DataPropertyName = "Descripcion";
            this.ColumnaDescripcion.HeaderText = "Descripción";
            this.ColumnaDescripcion.Name = "ColumnaDescripcion";
            this.ColumnaDescripcion.ReadOnly = true;
            this.ColumnaDescripcion.Width = 88;
            // 
            // ColumnaFechaInicio
            // 
            this.ColumnaFechaInicio.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.ColumnaFechaInicio.DataPropertyName = "FechaInicio";
            dataGridViewCellStyle1.Format = "d";
            dataGridViewCellStyle1.NullValue = null;
            this.ColumnaFechaInicio.DefaultCellStyle = dataGridViewCellStyle1;
            this.ColumnaFechaInicio.HeaderText = "Fecha Inicio";
            this.ColumnaFechaInicio.Name = "ColumnaFechaInicio";
            this.ColumnaFechaInicio.ReadOnly = true;
            this.ColumnaFechaInicio.Width = 90;
            // 
            // ColumnaFechaFin
            // 
            this.ColumnaFechaFin.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.ColumnaFechaFin.DataPropertyName = "FechaFin";
            dataGridViewCellStyle2.Format = "d";
            dataGridViewCellStyle2.NullValue = null;
            this.ColumnaFechaFin.DefaultCellStyle = dataGridViewCellStyle2;
            this.ColumnaFechaFin.HeaderText = "Fecha Fin";
            this.ColumnaFechaFin.Name = "ColumnaFechaFin";
            this.ColumnaFechaFin.ReadOnly = true;
            this.ColumnaFechaFin.Width = 79;
            // 
            // ColumnaActivo
            // 
            this.ColumnaActivo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.ColumnaActivo.DataPropertyName = "Activo";
            this.ColumnaActivo.HeaderText = "¿Está Activo?";
            this.ColumnaActivo.Name = "ColumnaActivo";
            this.ColumnaActivo.ReadOnly = true;
            this.ColumnaActivo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnaActivo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ColumnaActivo.Width = 98;
            // 
            // tsBarra
            // 
            this.tsBarra.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNuevo,
            this.btnEditar,
            this.btnEliminar,
            this.btnBuscar,
            this.btnResfrescar});
            this.tsBarra.Location = new System.Drawing.Point(0, 0);
            this.tsBarra.Name = "tsBarra";
            this.tsBarra.Size = new System.Drawing.Size(684, 38);
            this.tsBarra.TabIndex = 7;
            // 
            // btnNuevo
            // 
            this.btnNuevo.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnNuevo.Image = ((System.Drawing.Image)(resources.GetObject("btnNuevo.Image")));
            this.btnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(48, 35);
            this.btnNuevo.Text = "Nuevo";
            this.btnNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnEditar.Image = ((System.Drawing.Image)(resources.GetObject("btnEditar.Image")));
            this.btnEditar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(43, 35);
            this.btnEditar.Text = "Editar";
            this.btnEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnEliminar.Image = ((System.Drawing.Image)(resources.GetObject("btnEliminar.Image")));
            this.btnEliminar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(55, 35);
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnEliminar.ToolTipText = "Eliminar";
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnBuscar.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.Image")));
            this.btnBuscar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(48, 35);
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // btnResfrescar
            // 
            this.btnResfrescar.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnResfrescar.Image = ((System.Drawing.Image)(resources.GetObject("btnResfrescar.Image")));
            this.btnResfrescar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnResfrescar.Name = "btnResfrescar";
            this.btnResfrescar.Size = new System.Drawing.Size(65, 35);
            this.btnResfrescar.Text = "Refrescar";
            this.btnResfrescar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnResfrescar.Click += new System.EventHandler(this.btnResfrescar_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvPeriodos);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 38);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(684, 373);
            this.panel1.TabIndex = 8;
            // 
            // frmMantPeriodos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 411);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tsBarra);
            this.Name = "frmMantPeriodos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mantenimiento Períodos";
            this.Load += new System.EventHandler(this.frmMantPeriodos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPeriodos)).EndInit();
            this.tsBarra.ResumeLayout(false);
            this.tsBarra.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvPeriodos;
        private System.Windows.Forms.ToolStrip tsBarra;
        private System.Windows.Forms.ToolStripButton btnNuevo;
        private System.Windows.Forms.ToolStripButton btnEditar;
        private System.Windows.Forms.ToolStripButton btnEliminar;
        private System.Windows.Forms.ToolStripButton btnBuscar;
        private System.Windows.Forms.ToolStripButton btnResfrescar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Columna_Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnaDescripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnaFechaInicio;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnaFechaFin;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColumnaActivo;
    }
}