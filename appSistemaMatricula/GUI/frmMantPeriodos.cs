﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using appSistemaMatricula.BOL;
using sistemaMatricula.Entidades;

namespace appSistemaMatricula.GUI
{
    public partial class frmMantPeriodos : Form
    {
        private BolPeriodo bolPeriodo;
        public frmMantPeriodos()
        {
            this.bolPeriodo = new BolPeriodo();
            InitializeComponent();
        }

        private void frmMantPeriodos_Load(object sender, EventArgs e)
        {
            this.CargarPeriodos();
        }
        private void CargarPeriodos()
        {
            try
            {
                //this.dgvAulas.DataSource = null;
                this.dgvPeriodos.DataSource = this.bolPeriodo.ObtenerPeriodos();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            try
            {
                frmNuevoPeriodo frmNuevoPeriodo = new frmNuevoPeriodo();
                frmNuevoPeriodo.Text = "Crear Nuevo Periodo";
                if (frmNuevoPeriodo.ShowDialog() == DialogResult.OK)
                {
                    if (this.bolPeriodo.CrearPeriodo(frmNuevoPeriodo.oPeriodo))
                    {
                        MessageBox.Show("Datos agregados correctamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.CargarPeriodos();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvPeriodos.SelectedCells.Count > 0)
                {
                    frmNuevoPeriodo frmNuevoPeriodo = new frmNuevoPeriodo();
                    frmNuevoPeriodo.Text = "Editar Período";
                    Periodo oPeriodo = new Periodo();
                    DataGridViewSelectedCellCollection celdas = this.dgvPeriodos.SelectedCells;
                    oPeriodo.Id = Int32.Parse(celdas[0].Value.ToString());
                    oPeriodo.Descripcion = celdas[1].Value.ToString();
                    oPeriodo.FechaInicio = DateTime.Parse(celdas[2].Value.ToString());
                    oPeriodo.FechaFin = DateTime.Parse(celdas[3].Value.ToString());
                    oPeriodo.Activo = Boolean.Parse(celdas[4].Value.ToString());

                    frmNuevoPeriodo.oPeriodo = oPeriodo;
                    frmNuevoPeriodo.esEdicion = true;

                    if (frmNuevoPeriodo.ShowDialog() == DialogResult.OK)
                    {
                        if (this.bolPeriodo.ActualizarPerido(frmNuevoPeriodo.oPeriodo))
                        {
                            MessageBox.Show("Datos guardados correctamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.CargarPeriodos();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Operacion Cancelada", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Ningún registro seleccionado", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvPeriodos.SelectedCells.Count > 0)
                {
                    if (DialogResult.OK == MessageBox.Show("¿Realmente desea eliminar el registro?",
                    "", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning))
                    {
                        DataGridViewSelectedCellCollection celdas = this.dgvPeriodos.SelectedCells;
                        Periodo oPeriodo = new Periodo();
                        oPeriodo.Id = Int32.Parse(celdas[0].Value.ToString());
                        if (this.bolPeriodo.EliminarPeriodo(oPeriodo))
                        {
                            MessageBox.Show("Registro eliminado", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.CargarPeriodos();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Ningún registro seleccionado", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvPeriodos.Rows.Count > 0)
                {
                    frmNuevoPeriodo frmNuevoPeriodo = new frmNuevoPeriodo();
                    frmNuevoPeriodo.Text = "Buscar Período";
                    if (frmNuevoPeriodo.ShowDialog() == DialogResult.OK)
                    {
                        //this.dgvAulas.DataSource = null;
                        this.dgvPeriodos.DataSource = this.bolPeriodo.BuscarPeriodos(frmNuevoPeriodo.oPeriodo);
                    }
                }
                else
                {
                    MessageBox.Show("Sin registros para buscar", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnResfrescar_Click(object sender, EventArgs e)
        {
            this.CargarPeriodos();
        }
    }
}
