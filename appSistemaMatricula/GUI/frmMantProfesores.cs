﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using appSistemaMatricula.BOL;
using sistemaMatricula.Entidades;

namespace appSistemaMatricula.GUI
{
    public partial class frmMantProfesores : Form
    {
        private BolProfesor bolProfesor;
        public frmMantProfesores()
        {
            this.bolProfesor = new BolProfesor();
            InitializeComponent();
        }

        private void frmMantProfesores_Load(object sender, EventArgs e)
        {
            this.CargarProfesores();
        }

        /// <summary>
        /// Recarga el DataGridView que muestra los datos
        /// </summary>
        private void CargarProfesores()
        {
            try
            {
                this.dgvProfesores.DataSource = this.bolProfesor.CargarProfesores();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            try
            {
                frmNuevoProfesor frmNuevoEstudiante = new frmNuevoProfesor();
                frmNuevoEstudiante.Text = "Crear Nuevo Profesor";
                if (frmNuevoEstudiante.ShowDialog() == DialogResult.OK)
                {
                    if (this.bolProfesor.ValidarCrearActualizar(frmNuevoEstudiante.oProfesor))
                    {
                        MessageBox.Show("Datos agregados correctamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.CargarProfesores();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvProfesores.SelectedCells.Count > 0)
                {
                    frmNuevoProfesor frmNuevoProfesor = new frmNuevoProfesor();
                    frmNuevoProfesor.Text = "Editar Profesor";
                    Profesor oProfesor = new Profesor();
                    Carrera oCarrera = new Carrera();
                    Usuario oUsuario = new Usuario();

                    DataGridViewCellCollection celdas = this.dgvProfesores.CurrentRow.Cells;

                    oProfesor.Id = Int32.Parse(celdas["ColumnaId"].Value.ToString());
                    oProfesor.Nombre = celdas["ColumnaNombre"].Value.ToString();
                    oProfesor.Cedula = celdas["ColumnaCedula"].Value.ToString();
                    oProfesor.Email = celdas["ColumnaEmail"].Value.ToString();
                    oCarrera = (Carrera)celdas["ColumnaCarrera"].Value;
                    oUsuario = (Usuario)celdas["ColumnaUsuario"].Value;
                    //oEstudiante.EstaActivo = Boolean.Parse(celdas["ColumnaActivo"].Value.ToString());
                    //oEstudiante.Promedio = Double.Parse(celdas["ColumnaPromedio"].Value.ToString());
                    oProfesor.Carrera = oCarrera;
                    oProfesor.Usuario = oUsuario;

                    frmNuevoProfesor.oProfesor = oProfesor;
                    frmNuevoProfesor.esEdicion = true;

                    if (frmNuevoProfesor.ShowDialog() == DialogResult.OK)
                    {
                        if (this.bolProfesor.ValidarCrearActualizar(frmNuevoProfesor.oProfesor))
                        {
                            MessageBox.Show("Datos guardados correctamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.CargarProfesores();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Operacion Cancelada", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Ningún registro seleccionado", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvProfesores.SelectedCells.Count > 0)
                {
                    if (DialogResult.OK == MessageBox.Show("¿Realmente desea eliminar el registro?",
                    "", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning))
                    {
                        DataGridViewCellCollection celdas = this.dgvProfesores.CurrentRow.Cells;
                        Profesor oProfesor = new Profesor();
                        oProfesor.Id = Int32.Parse(celdas["ColumnaId"].Value.ToString());
                        if (this.bolProfesor.EliminarProfesor(oProfesor))
                        {
                            MessageBox.Show("Registro eliminado", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.CargarProfesores();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Ningún registro seleccionado", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvProfesores.Rows.Count > 0)
                {
                    Profesor oProfesor = new Profesor();
                    frmBuscarPersona frmBuscarPersona = new frmBuscarPersona(oProfesor);
                    frmBuscarPersona.Text = "Buscar Profesor";
                    if (frmBuscarPersona.ShowDialog() == DialogResult.OK)
                    {
                        this.dgvProfesores.DataSource = this.bolProfesor.CargarProfesores(oProfesor);
                    }
                }
                else
                {
                    MessageBox.Show("Sin registros para buscar", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnResfrescar_Click(object sender, EventArgs e)
        {
            this.CargarProfesores();
        }
    }
}
