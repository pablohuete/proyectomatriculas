﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using sistemaMatricula.Entidades;
using sistemaMatricula.Repositorios;

namespace appSistemaMatricula.GUI
{
    public partial class frmMantUsuarios : Form
    {
        private BolUsuario BolUsuario;
        public frmMantUsuarios()
        {
            this.BolUsuario = new BolUsuario();
            InitializeComponent();
        }
        private void frmMantUsuarios_Load(object sender, EventArgs e)
        {
            this.CagarUsuarios();
        }
        /// <summary>
        /// Recarga el DataGridView que muestra los datos
        /// </summary>
        private void CagarUsuarios()
        {
            try
            {                
                this.dgvUsuarios.DataSource = this.BolUsuario.CargarUsuarios();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            try
            {
                frmNuevoUsuario frmNuevoUsuario = new frmNuevoUsuario();
                frmNuevoUsuario.Text = "Crear Nuevo Usuario";
                if (frmNuevoUsuario.ShowDialog() == DialogResult.OK)
                {
                    if (this.BolUsuario.ValidarCrearActualizar(frmNuevoUsuario.oUsuario, frmNuevoUsuario.repetirContrasena))
                    {
                        MessageBox.Show("Datos agregados correctamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.CagarUsuarios();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); ;
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvUsuarios.SelectedCells.Count > 0)
                {
                    frmNuevoUsuario frmNuevoUsuario = new frmNuevoUsuario();
                    frmNuevoUsuario.Text = "Editar Usuario";
                    Usuario oUsuario = new Usuario();
                    DataGridViewSelectedCellCollection celdas = this.dgvUsuarios.SelectedCells;
                    oUsuario.Id = Int32.Parse(celdas[0].Value.ToString());
                    oUsuario.NombreUsuario = celdas[1].Value.ToString();
                    oUsuario.Contrasena = Seguridad.Desencriptar(celdas[2].Value.ToString());
                    oUsuario.EsAdmin = Boolean.Parse(celdas[3].Value.ToString());

                    frmNuevoUsuario.oUsuario = oUsuario;

                    if (frmNuevoUsuario.ShowDialog() == DialogResult.OK)
                    {
                        if (this.BolUsuario.ValidarCrearActualizar(frmNuevoUsuario.oUsuario, frmNuevoUsuario.repetirContrasena))
                        {
                            MessageBox.Show("Datos guardados correctamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.CagarUsuarios();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Operacion Cancelada", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Ningún registro seleccionado", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvUsuarios.SelectedCells.Count > 0)
                {
                    if (DialogResult.OK == MessageBox.Show("¿Realmente desea eliminar el registro?",
                    "", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning))
                    {
                        DataGridViewSelectedCellCollection celdas = this.dgvUsuarios.SelectedCells;
                        Usuario oUsuario = new Usuario();
                        oUsuario.Id = Int32.Parse(celdas[0].Value.ToString());
                        if (this.BolUsuario.EliminarUsuario(oUsuario))
                        {
                            MessageBox.Show("Registro eliminado", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.CagarUsuarios();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Ningún registro seleccionado", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvUsuarios.Rows.Count > 0)
                {
                    frmNuevoUsuario frmNuevoUsuario = new frmNuevoUsuario();
                    frmNuevoUsuario.Text = "Buscar Usuario";
                    frmNuevoUsuario.esBusqueda = true;
                    if (frmNuevoUsuario.ShowDialog() == DialogResult.OK)
                    {                        
                        this.dgvUsuarios.DataSource = this.BolUsuario.CargarUsuarios(frmNuevoUsuario.oUsuario.NombreUsuario);
                    }
                }
                else
                {
                    MessageBox.Show("Sin registros para buscar", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnResfrescar_Click(object sender, EventArgs e)
        {
            this.CagarUsuarios();
        }
    }
}
