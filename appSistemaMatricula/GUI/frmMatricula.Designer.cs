﻿namespace appSistemaMatricula.GUI
{
    partial class frmMatricula
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMatricula));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gbFiltros = new System.Windows.Forms.GroupBox();
            this.tsBarra = new System.Windows.Forms.ToolStrip();
            this.btnNuevo = new System.Windows.Forms.ToolStripButton();
            this.btnEditar = new System.Windows.Forms.ToolStripButton();
            this.btnEliminar = new System.Windows.Forms.ToolStripButton();
            this.btnResfrescar = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvEstudiantes = new System.Windows.Forms.DataGridView();
            this.ColumnaId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnaEstudiante = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnaCedula = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnaFechaHora = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnaPromedio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnInforme = new System.Windows.Forms.ToolStripButton();
            this.tsBarra.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstudiantes)).BeginInit();
            this.SuspendLayout();
            // 
            // gbFiltros
            // 
            this.gbFiltros.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbFiltros.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbFiltros.Location = new System.Drawing.Point(0, 0);
            this.gbFiltros.Name = "gbFiltros";
            this.gbFiltros.Size = new System.Drawing.Size(684, 119);
            this.gbFiltros.TabIndex = 0;
            this.gbFiltros.TabStop = false;
            // 
            // tsBarra
            // 
            this.tsBarra.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNuevo,
            this.btnEditar,
            this.btnEliminar,
            this.btnResfrescar,
            this.btnInforme});
            this.tsBarra.Location = new System.Drawing.Point(0, 119);
            this.tsBarra.Name = "tsBarra";
            this.tsBarra.Size = new System.Drawing.Size(684, 38);
            this.tsBarra.TabIndex = 8;
            // 
            // btnNuevo
            // 
            this.btnNuevo.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnNuevo.Image = ((System.Drawing.Image)(resources.GetObject("btnNuevo.Image")));
            this.btnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(48, 35);
            this.btnNuevo.Text = "Nuevo";
            this.btnNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // btnEditar
            // 
            this.btnEditar.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnEditar.Image = ((System.Drawing.Image)(resources.GetObject("btnEditar.Image")));
            this.btnEditar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(43, 35);
            this.btnEditar.Text = "Editar";
            this.btnEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // btnEliminar
            // 
            this.btnEliminar.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnEliminar.Image = ((System.Drawing.Image)(resources.GetObject("btnEliminar.Image")));
            this.btnEliminar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(55, 35);
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnEliminar.ToolTipText = "Eliminar";
            // 
            // btnResfrescar
            // 
            this.btnResfrescar.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnResfrescar.Image = ((System.Drawing.Image)(resources.GetObject("btnResfrescar.Image")));
            this.btnResfrescar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnResfrescar.Name = "btnResfrescar";
            this.btnResfrescar.Size = new System.Drawing.Size(65, 35);
            this.btnResfrescar.Text = "Refrescar";
            this.btnResfrescar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvEstudiantes);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 157);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(684, 254);
            this.panel1.TabIndex = 9;
            // 
            // dgvEstudiantes
            // 
            this.dgvEstudiantes.AllowUserToAddRows = false;
            this.dgvEstudiantes.AllowUserToDeleteRows = false;
            this.dgvEstudiantes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnaId,
            this.ColumnaEstudiante,
            this.ColumnaCedula,
            this.ColumnaFechaHora,
            this.ColumnaPromedio});
            this.dgvEstudiantes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvEstudiantes.Location = new System.Drawing.Point(0, 0);
            this.dgvEstudiantes.MultiSelect = false;
            this.dgvEstudiantes.Name = "dgvEstudiantes";
            this.dgvEstudiantes.ReadOnly = true;
            this.dgvEstudiantes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEstudiantes.Size = new System.Drawing.Size(684, 254);
            this.dgvEstudiantes.TabIndex = 8;
            // 
            // ColumnaId
            // 
            this.ColumnaId.DataPropertyName = "Id";
            this.ColumnaId.HeaderText = "Id";
            this.ColumnaId.Name = "ColumnaId";
            this.ColumnaId.ReadOnly = true;
            this.ColumnaId.Visible = false;
            // 
            // ColumnaEstudiante
            // 
            this.ColumnaEstudiante.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.ColumnaEstudiante.DataPropertyName = "Estudiante";
            this.ColumnaEstudiante.HeaderText = "Estudiante";
            this.ColumnaEstudiante.Name = "ColumnaEstudiante";
            this.ColumnaEstudiante.ReadOnly = true;
            this.ColumnaEstudiante.Width = 82;
            // 
            // ColumnaCedula
            // 
            this.ColumnaCedula.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.ColumnaCedula.DataPropertyName = "Periodo";
            this.ColumnaCedula.HeaderText = "Período";
            this.ColumnaCedula.Name = "ColumnaCedula";
            this.ColumnaCedula.ReadOnly = true;
            this.ColumnaCedula.Width = 70;
            // 
            // ColumnaFechaHora
            // 
            this.ColumnaFechaHora.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.ColumnaFechaHora.DataPropertyName = "FechaHora";
            dataGridViewCellStyle2.Format = "t";
            this.ColumnaFechaHora.DefaultCellStyle = dataGridViewCellStyle2;
            this.ColumnaFechaHora.HeaderText = "Fecha y Hora";
            this.ColumnaFechaHora.Name = "ColumnaFechaHora";
            this.ColumnaFechaHora.ReadOnly = true;
            this.ColumnaFechaHora.Width = 96;
            // 
            // ColumnaPromedio
            // 
            this.ColumnaPromedio.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.ColumnaPromedio.DataPropertyName = "Costo";
            this.ColumnaPromedio.HeaderText = "Costo";
            this.ColumnaPromedio.Name = "ColumnaPromedio";
            this.ColumnaPromedio.ReadOnly = true;
            this.ColumnaPromedio.Width = 59;
            // 
            // btnInforme
            // 
            this.btnInforme.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnInforme.Image = ((System.Drawing.Image)(resources.GetObject("btnInforme.Image")));
            this.btnInforme.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnInforme.Name = "btnInforme";
            this.btnInforme.Size = new System.Drawing.Size(57, 35);
            this.btnInforme.Text = "Informe";
            this.btnInforme.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // frmMatricula
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 411);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tsBarra);
            this.Controls.Add(this.gbFiltros);
            this.MinimumSize = new System.Drawing.Size(700, 450);
            this.Name = "frmMatricula";
            this.Text = "Matrícula";
            this.tsBarra.ResumeLayout(false);
            this.tsBarra.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstudiantes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbFiltros;
        private System.Windows.Forms.ToolStrip tsBarra;
        private System.Windows.Forms.ToolStripButton btnNuevo;
        private System.Windows.Forms.ToolStripButton btnEditar;
        private System.Windows.Forms.ToolStripButton btnEliminar;
        private System.Windows.Forms.ToolStripButton btnResfrescar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgvEstudiantes;
        private System.Windows.Forms.ToolStripButton btnInforme;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnaId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnaEstudiante;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnaCedula;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnaFechaHora;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnaPromedio;
    }
}