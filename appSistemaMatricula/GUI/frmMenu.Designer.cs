﻿namespace appSistemaMatricula.GUI
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiMantenimientos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiAulas = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCarreras = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCursos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEstudiantes = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiPeriodos = new System.Windows.Forms.ToolStripMenuItem();
            this.profesoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiRegistrarUsuarios = new System.Windows.Forms.ToolStripMenuItem();
            this.procesosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiGruposPeriodos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiAsignarNotas = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiMatricula = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cambioDeContraseñaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.parametrosDelSistemaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.tsmiMantenimientos,
            this.procesosToolStripMenuItem,
            this.helpMenu});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(601, 24);
            this.menuStrip.TabIndex = 3;
            this.menuStrip.Text = "MenuStrip";
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileMenu.ImageTransparentColor = System.Drawing.SystemColors.ActiveBorder;
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(60, 20);
            this.fileMenu.Text = "&Archivo";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(96, 22);
            this.exitToolStripMenuItem.Text = "&Salir";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // tsmiMantenimientos
            // 
            this.tsmiMantenimientos.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiAulas,
            this.tsmiCarreras,
            this.tsmiCursos,
            this.tsmiEstudiantes,
            this.tsmiPeriodos,
            this.profesoresToolStripMenuItem,
            this.tsmiRegistrarUsuarios});
            this.tsmiMantenimientos.Name = "tsmiMantenimientos";
            this.tsmiMantenimientos.Size = new System.Drawing.Size(106, 20);
            this.tsmiMantenimientos.Text = "&Mantenimientos";
            // 
            // tsmiAulas
            // 
            this.tsmiAulas.Name = "tsmiAulas";
            this.tsmiAulas.Size = new System.Drawing.Size(134, 22);
            this.tsmiAulas.Text = "Aulas";
            this.tsmiAulas.Click += new System.EventHandler(this.tsmiAulas_Click);
            // 
            // tsmiCarreras
            // 
            this.tsmiCarreras.Name = "tsmiCarreras";
            this.tsmiCarreras.Size = new System.Drawing.Size(134, 22);
            this.tsmiCarreras.Text = "Carreras";
            this.tsmiCarreras.Click += new System.EventHandler(this.tsmiCarreras_Click);
            // 
            // tsmiCursos
            // 
            this.tsmiCursos.Name = "tsmiCursos";
            this.tsmiCursos.Size = new System.Drawing.Size(134, 22);
            this.tsmiCursos.Text = "Cursos";
            this.tsmiCursos.Click += new System.EventHandler(this.tsmiCursos_Click);
            // 
            // tsmiEstudiantes
            // 
            this.tsmiEstudiantes.Name = "tsmiEstudiantes";
            this.tsmiEstudiantes.Size = new System.Drawing.Size(134, 22);
            this.tsmiEstudiantes.Text = "Estudiantes";
            this.tsmiEstudiantes.Click += new System.EventHandler(this.tsmiEstudiantes_Click);
            // 
            // tsmiPeriodos
            // 
            this.tsmiPeriodos.Name = "tsmiPeriodos";
            this.tsmiPeriodos.Size = new System.Drawing.Size(134, 22);
            this.tsmiPeriodos.Text = "Periodos";
            this.tsmiPeriodos.Click += new System.EventHandler(this.tsmiPeriodos_Click);
            // 
            // profesoresToolStripMenuItem
            // 
            this.profesoresToolStripMenuItem.Name = "profesoresToolStripMenuItem";
            this.profesoresToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.profesoresToolStripMenuItem.Text = "Profesores";
            this.profesoresToolStripMenuItem.Click += new System.EventHandler(this.profesoresToolStripMenuItem_Click);
            // 
            // tsmiRegistrarUsuarios
            // 
            this.tsmiRegistrarUsuarios.Name = "tsmiRegistrarUsuarios";
            this.tsmiRegistrarUsuarios.Size = new System.Drawing.Size(134, 22);
            this.tsmiRegistrarUsuarios.Text = "Usuarios";
            this.tsmiRegistrarUsuarios.Click += new System.EventHandler(this.tsmiRegistrarUsuario_Click);
            // 
            // procesosToolStripMenuItem
            // 
            this.procesosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiGruposPeriodos,
            this.tsmiAsignarNotas,
            this.tsmiMatricula});
            this.procesosToolStripMenuItem.Name = "procesosToolStripMenuItem";
            this.procesosToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.procesosToolStripMenuItem.Text = "&Procesos";
            // 
            // tsmiGruposPeriodos
            // 
            this.tsmiGruposPeriodos.Name = "tsmiGruposPeriodos";
            this.tsmiGruposPeriodos.Size = new System.Drawing.Size(255, 22);
            this.tsmiGruposPeriodos.Text = "Definición de Grupos por Periodos";
            this.tsmiGruposPeriodos.Click += new System.EventHandler(this.tsmiGruposPeriodos_Click);
            // 
            // tsmiAsignarNotas
            // 
            this.tsmiAsignarNotas.Name = "tsmiAsignarNotas";
            this.tsmiAsignarNotas.Size = new System.Drawing.Size(255, 22);
            this.tsmiAsignarNotas.Text = "Asignación de Notas";
            this.tsmiAsignarNotas.Click += new System.EventHandler(this.tsmiAsignarNotas_Click);
            // 
            // tsmiMatricula
            // 
            this.tsmiMatricula.Name = "tsmiMatricula";
            this.tsmiMatricula.Size = new System.Drawing.Size(255, 22);
            this.tsmiMatricula.Text = "Matrícula";
            this.tsmiMatricula.Click += new System.EventHandler(this.tsmiMatricula_Click);
            // 
            // helpMenu
            // 
            this.helpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator8,
            this.aboutToolStripMenuItem,
            this.cambioDeContraseñaToolStripMenuItem,
            this.parametrosDelSistemaToolStripMenuItem});
            this.helpMenu.Name = "helpMenu";
            this.helpMenu.Size = new System.Drawing.Size(53, 20);
            this.helpMenu.Text = "Ay&uda";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(194, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.aboutToolStripMenuItem.Text = "&Acerca de... ...";
            // 
            // cambioDeContraseñaToolStripMenuItem
            // 
            this.cambioDeContraseñaToolStripMenuItem.Name = "cambioDeContraseñaToolStripMenuItem";
            this.cambioDeContraseñaToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.cambioDeContraseñaToolStripMenuItem.Text = "Cambio de Contraseña";
            // 
            // parametrosDelSistemaToolStripMenuItem
            // 
            this.parametrosDelSistemaToolStripMenuItem.Name = "parametrosDelSistemaToolStripMenuItem";
            this.parametrosDelSistemaToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.parametrosDelSistemaToolStripMenuItem.Text = "Parametros del Sistema";
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(601, 423);
            this.Controls.Add(this.menuStrip);
            this.Name = "frmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menú Principal";
            this.Load += new System.EventHandler(this.frmMenu_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiMantenimientos;
        private System.Windows.Forms.ToolStripMenuItem tsmiPeriodos;
        private System.Windows.Forms.ToolStripMenuItem tsmiAulas;
        private System.Windows.Forms.ToolStripMenuItem tsmiCarreras;
        private System.Windows.Forms.ToolStripMenuItem tsmiCursos;
        private System.Windows.Forms.ToolStripMenuItem profesoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiEstudiantes;
        private System.Windows.Forms.ToolStripMenuItem procesosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiGruposPeriodos;
        private System.Windows.Forms.ToolStripMenuItem tsmiAsignarNotas;
        private System.Windows.Forms.ToolStripMenuItem tsmiMatricula;
        private System.Windows.Forms.ToolStripMenuItem helpMenu;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cambioDeContraseñaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem parametrosDelSistemaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiRegistrarUsuarios;
    }
}