﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using sistemaMatricula.Entidades;

namespace appSistemaMatricula.GUI
{
    public partial class frmMenu : Form
    {
        private Usuario eUsuario;
        
        public frmMenu(Usuario u)
        {
            this.eUsuario = u;
            InitializeComponent();
        }

        private void frmMenu_Load(object sender, EventArgs e)
        {
            //Para inabilitar los mantenimientos en caso que no sea Usuario root
            this.tsmiMantenimientos.Visible = this.eUsuario.EsAdmin;
        }

        private void tsmiAulas_Click(object sender, EventArgs e)
        {
            this.Hide();
            new frmMantAulas().ShowDialog();
            this.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void tsmiRegistrarUsuario_Click(object sender, EventArgs e)
        {
            this.Hide();
            new frmMantUsuarios().ShowDialog();
            this.Show();
        }

        private void tsmiPeriodos_Click(object sender, EventArgs e)
        {
            this.Hide();
            new frmMantPeriodos().ShowDialog();
            this.Show();
        }

        private void tsmiCarreras_Click(object sender, EventArgs e)
        {
            this.Hide();
            new frmMantCarreras().ShowDialog();
            this.Show();
        }

        private void tsmiEstudiantes_Click(object sender, EventArgs e)
        {
            this.Hide();
            new frmMantEstudiantes().ShowDialog();
            this.Show();
        }

        private void tsmiCursos_Click(object sender, EventArgs e)
        {
            this.Hide();
            new frmMantCursos().ShowDialog();
            this.Show();
        }

        private void profesoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            new frmMantProfesores().ShowDialog();
            this.Show();
        }

        private void tsmiGruposPeriodos_Click(object sender, EventArgs e)
        {
            this.Hide();
            new frmDefinicionGrupos().ShowDialog();
            this.Show();
        }

        private void tsmiAsignarNotas_Click(object sender, EventArgs e)
        {
            this.Hide();
            new frmAsignarNotas().ShowDialog();
            this.Show();
        }

        private void tsmiMatricula_Click(object sender, EventArgs e)
        {
            this.Hide();
            new frmMatricula().ShowDialog();
        }
    }
}
