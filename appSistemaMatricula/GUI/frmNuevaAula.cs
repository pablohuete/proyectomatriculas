﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using sistemaMatricula.Entidades;

namespace appSistemaMatricula.GUI
{
    public partial class frmNuevaAula : Form
    {
        public Aula oAula { get; set; }
        public frmNuevaAula()
        {
            this.oAula = new Aula();
            InitializeComponent();
        }

        private void frmNuevaAula_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(oAula.Descripcion) && !String.IsNullOrEmpty(oAula.Numero.ToString()))
            {
                this.txtDescripcion.Text = oAula.Descripcion;
                this.txtNumero.Text = oAula.Numero.ToString();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();            
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(this.txtDescripcion.Text))
                {
                    this.oAula.Descripcion = this.txtDescripcion.Text.Trim();
                }
                if (!String.IsNullOrEmpty(this.txtNumero.Text))
                {
                    this.oAula.Numero = Int32.Parse(this.txtNumero.Text.Trim());
                }
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
