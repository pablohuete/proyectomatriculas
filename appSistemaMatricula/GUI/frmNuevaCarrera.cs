﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using sistemaMatricula.Entidades;

namespace appSistemaMatricula.GUI
{
    public partial class frmNuevaCarrera : Form
    {
        public Carrera oCarrera { internal get; set; }
        public bool esEdicion { internal get; set; }

        public frmNuevaCarrera()
        {
            this.oCarrera = new Carrera();
            this.esEdicion = false;
            InitializeComponent();
        }

        private void frmNuevaCarrera_Load(object sender, EventArgs e)
        {
            try
            {
                if (esEdicion)
                {
                    this.txtCodigo.Text = oCarrera.CodigoCarrera;
                    this.txtDescripcion.Text = oCarrera.Descripcion;
                }                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Inserta en el objeto los datos de los controles
        /// </summary>
        private void ActualizarObjeto()
        {
            try
            {
                this.oCarrera.CodigoCarrera = this.txtCodigo.Text.Trim().ToUpper();
                this.oCarrera.Descripcion = this.txtDescripcion.Text.Trim();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                this.ActualizarObjeto();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
