﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using sistemaMatricula.Entidades;

namespace appSistemaMatricula.GUI
{
    public partial class frmNuevoCurso : Form
    {
        public Curso oCurso { get; internal set; }
        public bool esEdicion { get; internal set; }
        public frmNuevoCurso()
        {
            this.oCurso = new Curso();
            this.esEdicion = false;
            InitializeComponent();
        }

        private void frmNuevoCurso_Load(object sender, EventArgs e)
        {
            try
            {
                if (esEdicion)
                {
                    this.CargarControles();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// Carga los datos del objeto Curso en los controles
        /// cuando se edita
        /// </summary>
        private void CargarControles()
        {
            try
            {
                this.txtCodigo.Text = oCurso.CodigoCurso;
                this.txtDescripcion.Text = oCurso.Descripcion;
                this.txtCreditos.Text = oCurso.CantidadCreditos.ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Captura los datos digitados en los controles
        /// y los almacena en el objeto Curso
        /// </summary>
        /// <returns>true = capturados, false = no capturados</returns>
        private bool CapturarDatos()
        {
            bool capturados = false;
            try
            {
                this.oCurso.CodigoCurso = this.txtCodigo.Text.Trim().ToUpper();
                this.oCurso.Descripcion = this.txtDescripcion.Text.Trim();
                if (String.IsNullOrEmpty(this.txtCreditos.Text))
                    throw new Exception("Debe digitar los créditos");
                this.oCurso.CantidadCreditos = Int32.Parse(this.txtCreditos.Text.Trim());
                capturados = true;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al capturar datos: " + ex.Message);
            }            
            return capturados;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                if (CapturarDatos())
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
