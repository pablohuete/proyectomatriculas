﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using sistemaMatricula.Entidades;
using appSistemaMatricula.BOL;

namespace appSistemaMatricula.GUI
{
    public partial class frmNuevoEstudiante : Form
    {
        public Estudiante oEstudiante { internal get; set; }
        public bool esEdicion { get; internal set; }

        public frmNuevoEstudiante()
        {
            this.oEstudiante = new Estudiante();
            InitializeComponent();
        }

        private void frmNuevoEstudiante_Load(object sender, EventArgs e)
        {
            try
            {
                this.cbUsuarios.Visible = false;
                this.CargarComboCarreras();
                this.CargarComboUsuarios();
                if (esEdicion)
                {
                    this.CargarCamposControles();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// Llena los datos del estudiante a editar en los controles
        /// </summary>
        private void CargarCamposControles()
        {
            try
            {
                this.txtNombre.Text = this.oEstudiante.Nombre;
                this.txtCedula.Text = this.oEstudiante.Cedula;
                this.txtCorreo.Text = this.oEstudiante.Email;
                this.cbCarreras.SelectedValue = this.oEstudiante.Carrera.Id;
                this.txtPromedio.Text = this.oEstudiante.Promedio.ToString();
                this.cbxEstaActivo.Checked = this.oEstudiante.EstaActivo;
                //signo de pregunta para saber si es null                                           
                if (oEstudiante.Usuario?.Id > 0)
                {
                    this.cbxAsignarUsuario.Checked = true;
                    this.cbUsuarios.SelectedValue = this.oEstudiante.Usuario.Id;
                    this.cbUsuarios.Visible = true;
                }
                else
                {
                    this.cbUsuarios.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al cargar los datos del estudiante" + ex.Message);
            }
        }
        /// <summary>
        /// Carga el combox de las carreras
        /// </summary>
        private void CargarComboCarreras()
        {
            try
            {
                BolCarrera bolCarrera = new BolCarrera();
                this.cbCarreras.DataSource = bolCarrera.ObtenerCarreras();
            }
            catch (Exception ex)
            {
                throw new Exception("Error al cargar el combo box carreras: " + ex.Message);
            }
        }
        /// <summary>
        /// Cargar el combo box de los usuarios
        /// </summary>
        private void CargarComboUsuarios()
        {
            try
            {
                BolUsuario bolUsuario = new BolUsuario();
                this.cbUsuarios.DataSource = bolUsuario.CargarUsuarios();
            }
            catch (Exception ex)
            {
                throw new Exception("Error al cargar el combo box carreras: " + ex.Message);
            }
        }
        /// <summary>
        /// Carga los datos de los controles en el objeto Estudiante
        /// </summary>
        /// <returns>True = hecho, false = no hecho</returns>
        private bool CapturarDatos()
        {
            bool capturados = false;
            try
            {
                Carrera c = new Carrera();
                Usuario u = new Usuario();
                this.txtPromedio.ForeColor = Color.Black;

                this.oEstudiante.Nombre = this.txtNombre.Text.Trim();
                this.oEstudiante.Cedula = this.txtCedula.Text.Trim();
                this.oEstudiante.Email = this.txtCorreo.Text.Trim();
                if (this.cbCarreras.SelectedIndex != -1)
                {
                    c = (Carrera)this.cbCarreras.SelectedItem;
                }
                if (this.cbxAsignarUsuario.Checked)
                {
                    if (this.cbUsuarios.SelectedIndex != -1)
                    {
                        u = (Usuario)this.cbUsuarios.SelectedItem;
                    }
                }
                this.oEstudiante.EstaActivo = this.cbxEstaActivo.Checked;
                if (!Double.TryParse(this.txtPromedio.Text.Trim(), out double result) || result < 0)
                {
                    this.txtPromedio.Focus();
                    this.txtPromedio.Text = "Digite un número por favor";
                    this.txtPromedio.ForeColor = Color.Red;
                    throw new Exception("Campo Promedio  Inválido");
                }
                this.oEstudiante.Promedio = result;

                this.oEstudiante.Carrera = c;
                this.oEstudiante.Usuario = u;
                capturados = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return capturados;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (this.CapturarDatos())
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void cbxAsignarUsuario_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxAsignarUsuario.Checked)
            {
                this.cbxAsignarUsuario.Text = "Usuario Asignado";
                this.cbUsuarios.Visible = true;
            }
            else
            {
                this.cbxAsignarUsuario.Text = "Asignar Usuario";
                this.cbUsuarios.Visible = false;
            }
        }
    }
}
