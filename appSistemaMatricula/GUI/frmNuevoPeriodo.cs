﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using sistemaMatricula.Entidades;

namespace appSistemaMatricula.GUI
{
    public partial class frmNuevoPeriodo : Form
    {
        public Periodo oPeriodo { get; set; }
        public bool esEdicion { private get; set; }

        public frmNuevoPeriodo()
        {
            this.oPeriodo = new Periodo();
            this.esEdicion = false;
            InitializeComponent();
        }

        private void frmNuevoPeriodo_Load(object sender, EventArgs e)
        {
            try
            {
                if (esEdicion)
                {
                    this.txtDescripcion.Text = oPeriodo.Descripcion;
                    this.dtpFechaInicio.Value = oPeriodo.FechaInicio;
                    this.dtpFechaFin.Value = oPeriodo.FechaFin;
                    this.cbActivo.Checked = oPeriodo.Activo;
                }
                else
                {
                    this.dtpFechaInicio.Value = DateTime.Now;
                    this.dtpFechaFin.Value = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
        /// <summary>
        /// Actualiza las propiedades del objeto Periodo
        /// </summary>
        private void ActualizarObjeto()
        {
            try
            {
                this.oPeriodo.Descripcion = this.txtDescripcion.Text.Trim();
                if (dtpFechaInicio.Checked)
                {
                    this.oPeriodo.FechaInicio = this.dtpFechaInicio.Value.Date;
                }                
                if (dtpFechaFin.Checked)
                {
                    this.oPeriodo.FechaFin = this.dtpFechaFin.Value.Date;
                }
                if (dtpFechaInicio.Value.Date >= dtpFechaFin.Value.Date)
                {
                    throw new Exception("Fechas no correctas!");
                }
                this.oPeriodo.Activo = this.cbActivo.Checked;                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void dtpFechaInicio_CloseUp(object sender, EventArgs e)
        {
            try
            {
                this.ActualizarObjeto();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
        }

        private void dtpFechaFin_CloseUp(object sender, EventArgs e)
        {
            try
            {
                this.ActualizarObjeto();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbActivo_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.ActualizarObjeto();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                this.ActualizarObjeto();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
        }
    }
}
