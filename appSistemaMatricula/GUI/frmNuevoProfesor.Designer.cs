﻿namespace appSistemaMatricula.GUI
{
    partial class frmNuevoProfesor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbDatos = new System.Windows.Forms.GroupBox();
            this.panelControles = new System.Windows.Forms.Panel();
            this.cbxAsignarUsuario = new System.Windows.Forms.CheckBox();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.cbxEstaActivo = new System.Windows.Forms.CheckBox();
            this.txtCorreo = new System.Windows.Forms.TextBox();
            this.txtCedula = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.cbUsuarios = new System.Windows.Forms.ComboBox();
            this.cbCarreras = new System.Windows.Forms.ComboBox();
            this.panelEtiquetas = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gbDatos.SuspendLayout();
            this.panelControles.SuspendLayout();
            this.panelEtiquetas.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbDatos
            // 
            this.gbDatos.Controls.Add(this.panelControles);
            this.gbDatos.Controls.Add(this.panelEtiquetas);
            this.gbDatos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbDatos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbDatos.Location = new System.Drawing.Point(0, 0);
            this.gbDatos.Name = "gbDatos";
            this.gbDatos.Size = new System.Drawing.Size(422, 409);
            this.gbDatos.TabIndex = 1;
            this.gbDatos.TabStop = false;
            this.gbDatos.Text = "Datos";
            // 
            // panelControles
            // 
            this.panelControles.Controls.Add(this.cbxAsignarUsuario);
            this.panelControles.Controls.Add(this.btnAceptar);
            this.panelControles.Controls.Add(this.btnCancelar);
            this.panelControles.Controls.Add(this.cbxEstaActivo);
            this.panelControles.Controls.Add(this.txtCorreo);
            this.panelControles.Controls.Add(this.txtCedula);
            this.panelControles.Controls.Add(this.txtNombre);
            this.panelControles.Controls.Add(this.cbUsuarios);
            this.panelControles.Controls.Add(this.cbCarreras);
            this.panelControles.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControles.Location = new System.Drawing.Point(177, 17);
            this.panelControles.Name = "panelControles";
            this.panelControles.Size = new System.Drawing.Size(242, 389);
            this.panelControles.TabIndex = 1;
            // 
            // cbxAsignarUsuario
            // 
            this.cbxAsignarUsuario.AutoSize = true;
            this.cbxAsignarUsuario.Location = new System.Drawing.Point(42, 238);
            this.cbxAsignarUsuario.Name = "cbxAsignarUsuario";
            this.cbxAsignarUsuario.Size = new System.Drawing.Size(138, 20);
            this.cbxAsignarUsuario.TabIndex = 5;
            this.cbxAsignarUsuario.Text = "Asignar Usuario";
            this.cbxAsignarUsuario.UseVisualStyleBackColor = true;
            this.cbxAsignarUsuario.CheckedChanged += new System.EventHandler(this.cbxAsignarUsuario_CheckedChanged);
            // 
            // btnAceptar
            // 
            this.btnAceptar.Location = new System.Drawing.Point(127, 347);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(75, 23);
            this.btnAceptar.TabIndex = 8;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(14, 347);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(81, 23);
            this.btnCancelar.TabIndex = 7;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // cbxEstaActivo
            // 
            this.cbxEstaActivo.AutoSize = true;
            this.cbxEstaActivo.Location = new System.Drawing.Point(42, 212);
            this.cbxEstaActivo.Name = "cbxEstaActivo";
            this.cbxEstaActivo.Size = new System.Drawing.Size(121, 20);
            this.cbxEstaActivo.TabIndex = 5;
            this.cbxEstaActivo.Text = "¿Está Activo?";
            this.cbxEstaActivo.UseVisualStyleBackColor = true;
            this.cbxEstaActivo.Visible = false;
            // 
            // txtCorreo
            // 
            this.txtCorreo.Location = new System.Drawing.Point(3, 99);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.Size = new System.Drawing.Size(212, 21);
            this.txtCorreo.TabIndex = 2;
            // 
            // txtCedula
            // 
            this.txtCedula.Location = new System.Drawing.Point(3, 56);
            this.txtCedula.Name = "txtCedula";
            this.txtCedula.Size = new System.Drawing.Size(212, 21);
            this.txtCedula.TabIndex = 1;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(3, 15);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(212, 21);
            this.txtNombre.TabIndex = 0;
            // 
            // cbUsuarios
            // 
            this.cbUsuarios.DisplayMember = "NombreUsuario";
            this.cbUsuarios.FormattingEnabled = true;
            this.cbUsuarios.Location = new System.Drawing.Point(3, 289);
            this.cbUsuarios.Name = "cbUsuarios";
            this.cbUsuarios.Size = new System.Drawing.Size(212, 23);
            this.cbUsuarios.TabIndex = 6;
            this.cbUsuarios.ValueMember = "Id";
            // 
            // cbCarreras
            // 
            this.cbCarreras.DisplayMember = "CodigoCarrera";
            this.cbCarreras.FormattingEnabled = true;
            this.cbCarreras.Location = new System.Drawing.Point(3, 135);
            this.cbCarreras.Name = "cbCarreras";
            this.cbCarreras.Size = new System.Drawing.Size(212, 23);
            this.cbCarreras.TabIndex = 3;
            this.cbCarreras.ValueMember = "Id";
            // 
            // panelEtiquetas
            // 
            this.panelEtiquetas.Controls.Add(this.label4);
            this.panelEtiquetas.Controls.Add(this.label3);
            this.panelEtiquetas.Controls.Add(this.label2);
            this.panelEtiquetas.Controls.Add(this.label1);
            this.panelEtiquetas.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelEtiquetas.Location = new System.Drawing.Point(3, 17);
            this.panelEtiquetas.Name = "panelEtiquetas";
            this.panelEtiquetas.Size = new System.Drawing.Size(164, 389);
            this.panelEtiquetas.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(84, 135);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Carrera:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(84, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Correo:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(84, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Cédula:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(84, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre:";
            // 
            // frmNuevoProfesor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(422, 409);
            this.Controls.Add(this.gbDatos);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(438, 448);
            this.Name = "frmNuevoProfesor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Profesor";
            this.Load += new System.EventHandler(this.frmNuevoProfesor_Load);
            this.gbDatos.ResumeLayout(false);
            this.panelControles.ResumeLayout(false);
            this.panelControles.PerformLayout();
            this.panelEtiquetas.ResumeLayout(false);
            this.panelEtiquetas.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbDatos;
        private System.Windows.Forms.Panel panelControles;
        private System.Windows.Forms.CheckBox cbxAsignarUsuario;
        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.CheckBox cbxEstaActivo;
        private System.Windows.Forms.TextBox txtCorreo;
        private System.Windows.Forms.TextBox txtCedula;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.ComboBox cbUsuarios;
        private System.Windows.Forms.ComboBox cbCarreras;
        private System.Windows.Forms.Panel panelEtiquetas;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}