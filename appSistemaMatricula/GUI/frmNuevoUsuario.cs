﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using sistemaMatricula.Entidades;

namespace appSistemaMatricula.GUI
{
    public partial class frmNuevoUsuario : Form
    {
        public Usuario oUsuario { get; set; }
        public string repetirContrasena { get; set; }
        public bool esBusqueda { get; set; }
        public frmNuevoUsuario()
        {
            this.esBusqueda = false;
            this.oUsuario = new Usuario();
            InitializeComponent();
        }

        private void frmNuevoUsuario_Load(object sender, EventArgs e)
        {
            if (esBusqueda)
            {
                this.OcultarControles();                
            }
            else
            {
                if (!String.IsNullOrEmpty(this.oUsuario.NombreUsuario) &&
                !String.IsNullOrEmpty(this.oUsuario.Contrasena))
                {
                    this.txtUsuario.Text = this.oUsuario.NombreUsuario;
                    this.txtContrasena.Text = this.oUsuario.Contrasena;
                    if (this.oUsuario.EsAdmin)
                    {
                        this.cbTipo.SelectedText = "Administrador";
                    }
                    else
                    {
                        this.cbTipo.SelectedText = "Usuario";
                    }
                }
                else
                {
                    this.cbTipo.SelectedItem = "Usuario";
                }
            }
        }
        /// <summary>
        /// Oculta los controles al usuario cuando hace una búsqueda
        /// </summary>
        private void OcultarControles()
        {
            this.txtContrasena.Visible = false;
            this.txtRepetirContrasena.Visible = false;
            this.cbxMostrar.Visible = false;
            this.cbTipo.Visible = false;
            this.lblMensaje.Visible = false;
            this.lblContrasena.Visible = false;
            this.lblRepetirContrasena.Visible = false;
            this.lblTipo.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                if (esBusqueda)
                {
                    this.oUsuario.NombreUsuario = this.txtUsuario.Text;
                }
                else
                {
                    if (!String.IsNullOrEmpty(this.txtUsuario.Text))
                    {
                        this.oUsuario.NombreUsuario = this.txtUsuario.Text.Trim();
                    }
                    if (!String.IsNullOrEmpty(this.txtContrasena.Text))
                    {
                        this.oUsuario.Contrasena = this.txtContrasena.Text.Trim();
                    }
                    if (!String.IsNullOrEmpty(this.txtRepetirContrasena.Text))
                    {
                        this.repetirContrasena = this.txtRepetirContrasena.Text.Trim();
                    }
                    this.oUsuario.EsAdmin = (this.cbTipo.SelectedItem.ToString() == "Administrador");
                }

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbxMostrar_CheckedChanged(object sender, EventArgs e)
        {
            this.txtContrasena.UseSystemPasswordChar = !this.cbxMostrar.Checked;
        }
    }
}
