﻿using appSistemaMatricula.GUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace appSistemaMatricula
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmLogin());
            //try
            //{
            //    //Clave Normal
            //    string clave = "HolaMaría";
            //    Console.WriteLine("Clave normal: " + clave);
            //    Console.WriteLine("continuar");
            //    Console.ReadLine();

            //    Console.WriteLine("Aplicando el algorito para encriptar la clave");
            //    Console.ReadLine();
            //    //Encriptando
            //    string claveEncriptada = sistemaMatricula.Repositorios.Seguridad.Encriptar(clave);
            //    Console.WriteLine("Encriptada es: " + claveEncriptada);
            //    Console.ReadLine();
            //    //Desencriptando
            //    string claveDesencriptada = sistemaMatricula.Repositorios.Seguridad.Desencriptar(claveEncriptada);
            //    Console.WriteLine("Desencriptada es: " + claveDesencriptada);
            //    Console.ReadLine();
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}         
        }
    }
}
