--CREATE DATABASE dbsistema_matricula;

CREATE TABLE public.usuario
(
	id serial NOT NULL,
	nombre_usuario character varying(500) NOT NULL,
	contrasena character varying(500) NOT NULL,
	es_admin character varying(1) NOT NULL DEFAULT 'N',
	CONSTRAINT pk_usuario PRIMARY KEY (id),
	CONSTRAINT chk_es_admin CHECK (es_admin = 'S' or es_admin = 'N')
);

CREATE TABLE public.profesor
(
	id serial NOT NULL,
	nombre character varying(500) NOT NULL,
	cedula character varying(20) NOT NULL,
	email character varying(100),
	id_carrera integer,
	id_usuario integer,
	CONSTRAINT pk_profesor PRIMARY KEY (id)
);

CREATE TABLE public.carrera
(
	id serial NOT NULL,
	cod_carrera character varying(20),
	descripcion character varying(300),
	CONSTRAINT pk_carrera PRIMARY KEY (id)
);

CREATE TABLE public.estudiante
(
	id serial NOT NULL,
	nombre character varying(500) NOT NULL,
	cedula character varying(20) NOT NULL,
	email character varying(100),
	id_carrera integer,
	esta_activo character varying(1) NOT NULL DEFAULT 'N',
	promedio double precision NOT NULL DEFAULT 0,
	id_usuario integer,
	CONSTRAINT pk_estudiante PRIMARY KEY (id),
	CONSTRAINT chk_esta_activo CHECK (esta_activo = 'S' or esta_activo = 'N')
);

CREATE TABLE public.aula
(
	id serial NOT NULL,
	descripcion character varying(300) NOT NULL,
	numero integer NOT NULL,
	CONSTRAINT pk_aula PRIMARY KEY (id)
);

CREATE TABLE public.curso
(
	id serial NOT NULL,
	cod_curso character varying(100) NOT NULL,
	descripcion character varying(300),
	cantidad_creditos integer NOT NULL,
	CONSTRAINT pk_curso PRIMARY KEY (id)
);

CREATE TABLE public.grupo
(
	id serial NOT NULL,
	numero integer NOT NULL,
	dias_semana character varying (300) NOT NULL,
	hora_inicio time NOT NULL,
	hora_fin time NOT NULL,
	id_curso integer,
	cupo integer NOT NULL,
	id_profesor integer,
	nota_final_estudiante double precision NOT NULL DEFAULT 0,
	id_aula integer,
	esta_activo_estudiante character varying(1) NOT NULL DEFAULT 'N',
	CONSTRAINT pk_grupo PRIMARY KEY (id),
	CONSTRAINT chk_esta_activo_estudiante CHECK (esta_activo_estudiante = 'S' or esta_activo_estudiante = 'N')
);

CREATE TABLE public.grupo_matricula
(
	id serial NOT NULL,
	id_grupo integer NOT NULL,
	id_matricula integer NOT NULL,
	CONSTRAINT pk_grupo_matricula PRIMARY KEY (id)
);

CREATE TABLE public.matricula
(
	id serial NOT NULL,
	id_estudiante integer,
	id_periodo integer,
	fecha_hora timestamp without time zone NOT NULL,
	costo double precision NOT NULL DEFAULT 0,
	CONSTRAINT pk_matricula PRIMARY KEY (id)
);

CREATE TABLE public.periodo
(
	id serial NOT NULL,
	descripcion character varying(300) NOT NULL, 
	fecha_inicio timestamp without time zone NOT NULL,
	fecha_fin timestamp without time zone NOT NULL,
	activo character varying(1) NOT NULL DEFAULT 'N',
	CONSTRAINT pk_periodo PRIMARY KEY (id),
	CONSTRAINT chk_activo_periodo CHECK (activo = 'S' or activo = 'N')
);

CREATE TABLE public.recibo
(
	id serial NOT NULL,
	id_matricula integer,
	costo_seguro double precision,
	costo_matricula double precision NOT NULL DEFAULT 0,
	fecha_hora timestamp without time zone NOT NULL,
	CONSTRAINT pk_recibo PRIMARY KEY (id)
);

CREATE TABLE public.parametros
(
	id serial NOT NULL,
	costo_credito double precision NOT NULL,
	costo_seguro double precision NOT NULL,
	CONSTRAINT pk_parametros PRIMARY KEY (id)
);

--FOREIGN KEYS
--Foreign keys tabla profesor
ALTER TABLE public.profesor
  ADD CONSTRAINT fk_profesor_usuario FOREIGN KEY (id_usuario)
      REFERENCES public.usuario (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE public.profesor
  ADD CONSTRAINT fk_profesor_carrera FOREIGN KEY (id_carrera)
      REFERENCES public.carrera (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

--Foreign keys tabla estudiante
ALTER TABLE public.estudiante
  ADD CONSTRAINT fk_estudiante_usuario FOREIGN KEY (id_usuario)
      REFERENCES public.usuario (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
	  
ALTER TABLE public.estudiante
  ADD CONSTRAINT fk_estudiante_carrera FOREIGN KEY (id_carrera)
      REFERENCES public.carrera (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

--Foreign keys tabla matricula  
ALTER TABLE public.matricula
  ADD CONSTRAINT fk_matricula_estudiante FOREIGN KEY (id_estudiante)
      REFERENCES public.estudiante (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION; 
   
ALTER TABLE public.matricula
  ADD CONSTRAINT fk_matricula_periodo FOREIGN KEY (id_periodo)
      REFERENCES public.periodo (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

--Foreign keys tabla grupo
ALTER TABLE public.grupo
  ADD CONSTRAINT fk_grupo_curso FOREIGN KEY (id_curso)
      REFERENCES public.curso (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE public.grupo
  ADD CONSTRAINT fk_grupo_profesor FOREIGN KEY (id_profesor)
      REFERENCES public.profesor (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE public.grupo
  ADD CONSTRAINT fk_grupo_aula FOREIGN KEY (id_aula)
      REFERENCES public.aula (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

--Foreign keys tabla grupo_matricula
ALTER TABLE public.grupo_matricula
  ADD CONSTRAINT fk_grupo_matricula_matricula FOREIGN KEY (id_matricula)
      REFERENCES public.matricula (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
	  
ALTER TABLE public.grupo_matricula
  ADD CONSTRAINT fk_grupo_matricula_grupo FOREIGN KEY (id_grupo)
      REFERENCES public.grupo (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

--Foreign key recibo
ALTER TABLE public.recibo
  ADD CONSTRAINT fk_recibo_matricula FOREIGN KEY (id_matricula)
      REFERENCES public.matricula (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
	  

/*
	Usuario Por Defecto del sistema
* Nombre de Usuario = grethel
* Contraseña = holasoygrethel
*/
INSERT INTO public.usuario (nombre_usuario, contrasena, es_admin) VALUES ('grethel', '50gC5OopnWAcCUyVfqSYAA==', 'S');

/*
*	Parametros del sistema
*/
INSERT INTO public.parametros (costo_credito, costo_seguro) VALUES (12500.00, 14855.00);