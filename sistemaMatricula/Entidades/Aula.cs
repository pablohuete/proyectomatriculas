﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sistemaMatricula.Entidades
{
    public class Aula
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public int Numero { get; set; }

        public Aula(int pId, string pDescripcion, int pNumero)
        {
            this.Id = pId;
            this.Descripcion = pDescripcion;
            this.Numero = pNumero;
        }
        public Aula()
        {

        }

        public override string ToString()
        {
            return Numero.ToString() + " - " +Descripcion.ToString();
        }
    }
}
