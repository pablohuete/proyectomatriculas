﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sistemaMatricula.Entidades
{
    public class Carrera
    {
        public int Id { get; set; }
        public string CodigoCarrera { get; set; }
        public string Descripcion { get; set; }

        public override string ToString()
        {
            return this.CodigoCarrera.ToString();
        }
    }
}
