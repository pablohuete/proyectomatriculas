﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sistemaMatricula.Entidades
{
    public class Estudiante : Persona
    {
        public bool EstaActivo { get; set; }
        public double Promedio { get; set; }
    }
}
