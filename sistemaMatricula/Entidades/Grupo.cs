﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sistemaMatricula.Entidades
{
    public class Grupo
    {
        public int Id { get; set; }
        public int Numero { get; set; }
        public string DiasSemana { get; set; }
        public DateTime HoraInicio { get; set; }
        public DateTime HoraFin { get; set; }
        public Periodo Periodo { get; set; }
        public Curso Curso { get; set; }
        public int Cupos { get; set; }
        public Profesor Profesor { get; set; }
        public double NotaFinal { get; set; }
        public Aula Aula { get; set; }
        public bool EstaActivo { get; set; }
    }
}
