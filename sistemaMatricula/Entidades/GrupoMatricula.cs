﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sistemaMatricula.Entidades
{
    public class GrupoMatricula
    {
        public int Id { get; set; }
        public int IdGrupo { get; set; }
        public int IdMatricula { get; set; }
    }
}
