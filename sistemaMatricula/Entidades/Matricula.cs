﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sistemaMatricula.Entidades
{
    public class Matricula
    {
        public int Id { get; set; }
        public Estudiante Estudiante { get; set; }
        public Periodo Periodo { get; set; }
        public DateTime FechaHora { get; set; }
        public double Costo { get; set; }

        private List<Grupo> grupos;
    }
}
