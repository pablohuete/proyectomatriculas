﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sistemaMatricula.Entidades
{
    public class ParametroSistema
    {
        public int Id { get; set; }
        public double CostoCredito { get; set; }
        public double CostoSeguro { get; set; }
    }
}
