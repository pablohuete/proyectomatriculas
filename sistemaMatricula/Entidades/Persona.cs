﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sistemaMatricula.Entidades
{
    public abstract class Persona
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Cedula { get; set; }
        public string Email { get; set; }
        public Carrera Carrera { get; set; }
        public Usuario Usuario { get; set; }

        public override string ToString()
        {
            return this.Nombre.ToString();
        }
    }

}
