﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sistemaMatricula.Entidades
{
    public class Recibo
    {
        public int Id { get; set; }
        public int IdMatricula { get; set; }
        public double CostoSeguro { get; set; }
        public double CostoMatricula { get; set; }
        public DateTime FechaHora { get; set; }

    }
}
