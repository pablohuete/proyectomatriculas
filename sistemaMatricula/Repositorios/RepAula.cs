﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sistemaMatricula.Entidades;
using sistemaMatricula.Repositorios.Datos;
using NpgsqlTypes;
using System.Data;

namespace sistemaMatricula.Repositorios
{
    public class RepAula
    {
        /// <summary>
        /// Obtiene una lista con las aulas
        /// </summary>
        /// <returns>Arreglo de Aulas</returns>
        public List<Aula> ObtenerAulas()
        {
            List<Aula> aulas = new List<Aula>();

            AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
            DataSet dataSet;
            try
            {
                string sql = "SELECT * FROM " + cnx.Schema + ".aula ORDER BY id ASC";

                dataSet = cnx.ejecutarConsultaSQL(sql);
                if (dataSet.Tables.Count > 0)
                {
                    foreach (DataRow fila in dataSet.Tables[0].Rows)
                    {
                        aulas.Add(new Aula
                        {
                            Id = Int32.Parse(fila["id"].ToString()),
                            Descripcion = fila["descripcion"].ToString(),
                            Numero = Int32.Parse(fila["numero"].ToString())
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return aulas;
        }
        /// <summary>
        /// Busca aulas en la base de datos
        /// </summary>
        /// <param name="pAula">Entidad Aula</param>
        /// <returns>Lista de aulas filtrada</returns>
        public List<Aula> BuscarAulas(Aula pAula)
        {
            AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
            Parametro oParametro = new Parametro();
            DataSet dataSet;
            List<Aula> aulas = new List<Aula>();
            try
            {
                string sql = "SELECT * FROM " + cnx.Schema + ".aula WHERE descripcion LIKE @descripcion" +
                    " OR numero = @numero";

                oParametro.agregarParametro("@descripcion", NpgsqlDbType.Varchar, "%" + pAula.Descripcion);
                oParametro.agregarParametro("@numero", NpgsqlDbType.Integer, pAula.Numero);

                dataSet = cnx.ejecutarConsultaSQL(sql, "aula", oParametro.obtenerParametros());
                if (dataSet.Tables.Count > 0)
                {
                    foreach (DataRow fila in dataSet.Tables[0].Rows)
                    {
                        aulas.Add(new Aula
                        {
                            Id = Int32.Parse(fila["id"].ToString()),
                            Descripcion = fila["descripcion"].ToString(),
                            Numero = Int32.Parse(fila["numero"].ToString())
                        });
                    }                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return aulas;
        }
        /// <summary>
        /// Guarda un Objeto Aula en la base de datos
        /// </summary>
        /// <param name="oAula">Entidad Aula a Crear</param>
        /// <returns>True = Creada, False = No creada</returns>
        public bool CrearAula(Aula oAula)
        {
            try
            {
                AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
                Parametro oParametro = new Parametro();
                string sql = "INSERT INTO " + cnx.Schema + ".aula (descripcion, numero)" +
                    " VALUES (@descripcion, @numero);";

                oParametro.agregarParametro("@descripcion", NpgsqlDbType.Varchar, oAula.Descripcion);
                oParametro.agregarParametro("@numero", NpgsqlDbType.Integer, oAula.Numero);

                cnx.ejecutarSQL(sql, oParametro.obtenerParametros());
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Actualiza un objeto aula en la base de datos
        /// </summary>
        /// <param name="oAula">Entidad Aula</param>
        /// <returns>True = Actualizada, False = No actualizada</returns>
        public bool ActualizarAula(Aula oAula)
        {
            AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
            Parametro oParametro = new Parametro();
            try
            {
                string sql = "UPDATE " + cnx.Schema + ".aula" +
                    " SET descripcion = @descripcion, numero = @numero WHERE id = @id;";

                oParametro.agregarParametro("@descripcion", NpgsqlDbType.Varchar, oAula.Descripcion);
                oParametro.agregarParametro("@numero", NpgsqlDbType.Integer, oAula.Numero);
                oParametro.agregarParametro("@id", NpgsqlDbType.Integer, oAula.Id);

                cnx.ejecutarSQL(sql, oParametro.obtenerParametros());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }
        /// <summary>
        /// Elimina un objeto Aula en la base de datos
        /// </summary>
        /// <param name="oAula">Entidad Aula</param>
        /// <returns>True = Eliminada, False = No eliminada</returns>
        public bool EliminarAula(Aula oAula)
        {
            AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
            Parametro oParametro = new Parametro();
            try
            {
                string sql = "DELETE FROM " + cnx.Schema + ".aula" +
                    " WHERE id = @id";

                oParametro.agregarParametro("@id", NpgsqlDbType.Integer, oAula.Id);

                cnx.ejecutarSQL(sql, oParametro.obtenerParametros());
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
