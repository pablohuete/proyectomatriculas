﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using NpgsqlTypes;
using sistemaMatricula.Repositorios.Datos;
using sistemaMatricula.Entidades;

namespace sistemaMatricula.Repositorios
{
    public class RepCarrera
    {
        /// <summary>
        /// Obtiene una lista con las Carreras
        /// </summary>
        /// <returns>Arreglo de Carreras</returns>
        public List<Carrera> ObtenerCarreras()
        {
            List<Carrera> carreras = new List<Carrera>();

            AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
            DataSet dataSet;
            try
            {
                string sql = "SELECT * FROM " + cnx.Schema + ".carrera ORDER BY cod_carrera ASC";

                dataSet = cnx.ejecutarConsultaSQL(sql);
                if (dataSet.Tables.Count > 0)
                {
                    foreach (DataRow fila in dataSet.Tables[0].Rows)
                    {
                        carreras.Add(new Carrera
                        {
                            Id = Int32.Parse(fila["id"].ToString()),
                            CodigoCarrera = fila["cod_carrera"].ToString(),                            
                            Descripcion = fila["descripcion"].ToString()                            
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return carreras;
        }
        /// <summary>
        /// Busca carreras en la base de datos
        /// </summary>
        /// <param name="oCarrera">Entidad Carrera</param>
        /// <returns>Lista de Carreras filtrada</returns>
        public List<Carrera> BuscarCarreras(Carrera oCarrera)
        {
            AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
            Parametro oParametro = new Parametro();
            DataSet dataSet;
            List<Carrera> carreras = new List<Carrera>();
            try
            {
                string sql = "SELECT * FROM " + cnx.Schema + ".carrera" +
                    " WHERE cod_carrera LIKE @cod_carrera" +
                    " OR descripcion LIKE @descripcion";                  

                oParametro.agregarParametro("@cod_carrera", NpgsqlDbType.Varchar, "%" + oCarrera.CodigoCarrera + "%");                
                oParametro.agregarParametro("@descripcion", NpgsqlDbType.Varchar, "%" + oCarrera.Descripcion + "%");                
                
                dataSet = cnx.ejecutarConsultaSQL(sql, "carrera", oParametro.obtenerParametros());
                if (dataSet.Tables.Count > 0)
                {
                    foreach (DataRow fila in dataSet.Tables[0].Rows)
                    {
                        carreras.Add(new Carrera
                        {
                            Id = Int32.Parse(fila["id"].ToString()),
                            CodigoCarrera = fila["cod_carrera"].ToString(),                            
                            Descripcion = fila["descripcion"].ToString()                            
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return carreras;
        }
        /// <summary>
        /// Guarda un Objeto Carrera en la base de datos
        /// </summary>
        /// <param name="oCarrera">Entidad Carrera a Crear</param>
        /// <returns>True = Creada, False = No creada</returns>
        public bool CrearCarrera(Carrera oCarrera)
        {
            bool creada = false;
            try
            {
                AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
                Parametro oParametro = new Parametro();
                string sql = "INSERT INTO " + cnx.Schema + ".carrera (cod_carrera, descripcion)" +
                    " VALUES (@cod_carrera, @descripcion);";

                oParametro.agregarParametro("@cod_carrera", NpgsqlDbType.Varchar, oCarrera.CodigoCarrera);
                oParametro.agregarParametro("@descripcion", NpgsqlDbType.Varchar, oCarrera.Descripcion);
                                
                cnx.ejecutarSQL(sql, oParametro.obtenerParametros());
                creada = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return creada;
        }
        /// <summary>
        /// Actualiza un objeto Carrera en la base de datos
        /// </summary>
        /// <param name="oCarrera">Entidad Carrera</param>
        /// <returns>True = Actualizada, False = No actualizada</returns>
        public bool ActualizarCarrera(Carrera oCarrera)
        {
            bool actualizada = false;
            try
            {
                AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
                Parametro oParametro = new Parametro();
                string sql = "UPDATE " + cnx.Schema + ".carrera" +
                    " SET cod_carrera = @cod_carrera, descripcion = @descripcion" +
                    " WHERE id = @id;";

                oParametro.agregarParametro("@cod_carrera", NpgsqlDbType.Varchar, oCarrera.CodigoCarrera);               
                oParametro.agregarParametro("@descripcion", NpgsqlDbType.Varchar, oCarrera.Descripcion);                               
                oParametro.agregarParametro("@id", NpgsqlDbType.Integer, oCarrera.Id);

                cnx.ejecutarSQL(sql, oParametro.obtenerParametros());
                actualizada = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return actualizada;
        }
        /// <summary>
        /// Elimina un objeto Carrera en la base de datos
        /// </summary>
        /// <param name="oCarrera">Entidad Carrera</param>
        /// <returns>True = Eliminada, False = No eliminada</returns>
        public bool EliminarCarrera(Carrera oCarrera)
        {
            AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
            Parametro oParametro = new Parametro();
            try
            {
                string sql = "DELETE FROM " + cnx.Schema + ".carrera" + " WHERE id = @id";

                oParametro.agregarParametro("@id", NpgsqlDbType.Integer, oCarrera.Id);

                cnx.ejecutarSQL(sql, oParametro.obtenerParametros());
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
