﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sistemaMatricula.Entidades;
using sistemaMatricula.Repositorios.Datos;
using NpgsqlTypes;
using System.Data;

namespace sistemaMatricula.Repositorios
{
    public class RepCurso
    {
        /// <summary>
        /// Obtiene una lista con los Cursos
        /// </summary>
        /// <returns>Arreglo de Cursos</returns>
        public List<Curso> ObtenerCursos()
        {
            List<Curso> cursos = new List<Curso>();            
            try
            {
                AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
                DataSet dataSet;

                string sql = "SELECT * FROM " + cnx.Schema + ".curso ORDER BY id ASC";

                dataSet = cnx.ejecutarConsultaSQL(sql);
                if (dataSet.Tables.Count > 0)
                {
                    foreach (DataRow fila in dataSet.Tables[0].Rows)
                    {
                        cursos.Add(new Curso
                        {
                            Id = Int32.Parse(fila["id"].ToString()),
                            CodigoCurso = fila["cod_curso"].ToString(),
                            Descripcion = fila["descripcion"].ToString(),
                            CantidadCreditos = Int32.Parse(fila["cantidad_creditos"].ToString())
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return cursos;
        }
        /// <summary>
        /// Busca cursos en la base de datos
        /// </summary>
        /// <param name="oCurso">Entidad Curso</param>
        /// <returns>Lista de Cursos filtrada</returns>
        public List<Curso> BuscarCursos(Curso oCurso)
        {
            List<Curso> cursos = new List<Curso>();
            try
            {
                AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
                Parametro oParametro = new Parametro();
                DataSet dataSet;                

                string sql = "SELECT * FROM " + cnx.Schema + ".curso WHERE cod_curso LIKE @cod_curso" +
                    " OR descripcion like @descripcion";

                oParametro.agregarParametro("@cod_curso", NpgsqlDbType.Varchar, "%" + oCurso.CodigoCurso);
                oParametro.agregarParametro("@descripcion", NpgsqlDbType.Varchar, "%" + oCurso.Descripcion);

                dataSet = cnx.ejecutarConsultaSQL(sql, "curso", oParametro.obtenerParametros());
                if (dataSet.Tables.Count > 0)
                {
                    foreach (DataRow fila in dataSet.Tables[0].Rows)
                    {
                        cursos.Add(new Curso
                        {
                            Id = Int32.Parse(fila["id"].ToString()),
                            CodigoCurso = fila["cod_curso"].ToString(),
                            Descripcion = fila["descripcion"].ToString(),
                            CantidadCreditos = Int32.Parse(fila["cantidad_creditos"].ToString())
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return cursos;
        }
        /// <summary>
        /// Guarda un Objeto Curso en la base de datos
        /// </summary>
        /// <param name="oCurso">Entidad Curso a Crear</param>
        /// <returns>True = Creado, False = No creado</returns>
        public bool CrearCurso(Curso oCurso)
        {
            bool creado = false;
            try
            {
                AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
                Parametro oParametro = new Parametro();
                string sql = "INSERT INTO " + cnx.Schema + ".curso (cod_curso, descripcion, cantidad_creditos)" +
                    " VALUES (@cod_curso, @descripcion, @cantidad_creditos);";

                oParametro.agregarParametro("@cod_curso", NpgsqlDbType.Varchar, oCurso.CodigoCurso);
                oParametro.agregarParametro("@descripcion", NpgsqlDbType.Varchar, oCurso.Descripcion);
                oParametro.agregarParametro("@cantidad_creditos", NpgsqlDbType.Integer, oCurso.CantidadCreditos);

                cnx.ejecutarSQL(sql, oParametro.obtenerParametros());
                creado = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return creado;
        }
        /// <summary>
        /// Actualiza un objeto curso en la base de datos
        /// </summary>
        /// <param name="oCurso">Entidad Curso</param>
        /// <returns>True = Actualizado, False = No actualizado</returns>
        public bool ActualizarCurso(Curso oCurso)
        {
            bool actualizado = false;
            try
            {
                AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
                Parametro oParametro = new Parametro();

                string sql = "UPDATE " + cnx.Schema + ".curso" +
                    " SET cod_curso = @cod_curso, descripcion = @descripcion, cantidad_creditos = @cantidad_creditos" +
                    " WHERE id = @id;";

                oParametro.agregarParametro("@cod_curso", NpgsqlDbType.Varchar, oCurso.CodigoCurso);
                oParametro.agregarParametro("@descripcion", NpgsqlDbType.Varchar, oCurso.Descripcion);
                oParametro.agregarParametro("@cantidad_creditos", NpgsqlDbType.Integer, oCurso.CantidadCreditos);
                oParametro.agregarParametro("@id", NpgsqlDbType.Integer, oCurso.Id);

                cnx.ejecutarSQL(sql, oParametro.obtenerParametros());
                actualizado = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return actualizado;
        }
        /// <summary>
        /// Elimina un objeto Curso en la base de datos
        /// </summary>
        /// <param name="oCurso">Entidad Curso</param>
        /// <returns>True = Eliminado, False = No eliminado</returns>
        public bool EliminarCurso(Curso oCurso)
        {
            bool eliminado = false; 
            try
            {
                AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
                Parametro oParametro = new Parametro();

                string sql = "DELETE FROM " + cnx.Schema + ".curso" +
                    " WHERE id = @id";

                oParametro.agregarParametro("@id", NpgsqlDbType.Integer, oCurso.Id);

                cnx.ejecutarSQL(sql, oParametro.obtenerParametros());
                eliminado = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return eliminado;
        }
    }
}
