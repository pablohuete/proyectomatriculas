﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sistemaMatricula.Entidades;
using System.Data;
using NpgsqlTypes;
using sistemaMatricula.Repositorios.Datos;

namespace sistemaMatricula.Repositorios
{
    public class RepEstudiante
    {
        /// <summary>
        /// Consulta los estudiantes guardados en la base de datos
        /// </summary>
        /// <returns>Lista de Estudiantes</returns>
        public List<Estudiante> CargarEstudiantes()
        {
            List<Estudiante> estudiantes = new List<Estudiante>();
            try
            {
                AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
                DataSet dataSet;

                string sql = "SELECT E.*, C.id AS c_id, C.cod_carrera, C.descripcion," +
                    " U.id AS u_id, U.nombre_usuario, U.contrasena, U.es_admin" +
                    " FROM " + cnx.Schema + ".estudiante E" +
                    " LEFT JOIN " + cnx.Schema + ".carrera C ON(E.id_carrera = C.id)" +
                    " LEFT JOIN " + cnx.Schema + ".usuario U ON(E.id_usuario = U.id)" +
                    " ORDER BY E.nombre ASC;";

                dataSet = cnx.ejecutarConsultaSQL(sql);
                if (dataSet.Tables.Count > 0)
                {
                    foreach (DataRow fila in dataSet.Tables[0].Rows)
                    {
                        Estudiante e = new Estudiante();
                        string idCarrera;
                        string idUsuario;
                        e.Id = Int32.Parse(fila["id"].ToString());
                        e.Nombre = fila["nombre"].ToString();
                        e.Cedula = fila["cedula"].ToString();
                        e.Email = fila["email"].ToString();
                        //Carrera del estudiante *si tiene*
                        idCarrera = fila["id_carrera"].ToString();
                        e.EstaActivo = fila["esta_activo"].ToString().Equals("S");
                        e.Promedio = Int32.Parse(fila["promedio"].ToString());
                        //Usuario del estudiante *si tiene*
                        idUsuario = fila["id_usuario"].ToString();

                        if (!String.IsNullOrEmpty(idCarrera))
                        {
                            Carrera c = new Carrera();
                            c.Id = Int32.Parse(fila["c_id"].ToString());
                            c.CodigoCarrera = fila["cod_carrera"].ToString();
                            c.Descripcion = fila["descripcion"].ToString();
                            //Le asigna la carrera
                            e.Carrera = c;
                        }

                        if (!String.IsNullOrEmpty(idUsuario))
                        {
                            Usuario u = new Usuario();
                            u.Id = Int32.Parse(fila["u_id"].ToString());
                            u.NombreUsuario = fila["nombre_usuario"].ToString();
                            u.Contrasena = Seguridad.Desencriptar(fila["contrasena"].ToString());
                            u.EsAdmin = fila["es_admin"].ToString().Equals("S");
                            //Le asigna el usuario
                            e.Usuario = u;
                        }
                        //lo agrega a la lista
                        estudiantes.Add(e);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return estudiantes;
        }
        /// <summary>
        /// Realiza una consulta filtrada de los estudiantes guardados en la base de datos
        /// </summary>
        /// <param name="oEstudiante">Entidad Estudiante</param>
        /// <returns>Lista filtrada de Estudiantes</returns>
        public List<Estudiante> CargarEstudiantes(Estudiante oEstudiante)
        {
            List<Estudiante> estudiantes = new List<Estudiante>();
            try
            {
                AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
                Parametro oParametro = new Parametro();
                DataSet dataSet;

                string sql = "SELECT E.*, C.id AS c_id, C.cod_carrera, C.descripcion," +
                    " U.id AS u_id, U.nombre_usuario, U.contrasena, U.es_admin" +
                    " FROM " + cnx.Schema + ".estudiante E" +
                    " LEFT JOIN " + cnx.Schema + ".carrera C ON(E.id_carrera = C.id)" +
                    " LEFT JOIN " + cnx.Schema + ".usuario U ON(E.id_usuario = U.id)" +
                    " WHERE E.cedula LIKE @cedula" + //filtro por cédula
                    " ORDER BY E.nombre ASC;";

                oParametro.agregarParametro("@cedula", NpgsqlDbType.Varchar, "%" + oEstudiante.Cedula + "%");
                dataSet = cnx.ejecutarConsultaSQL(sql, "estudiantes", oParametro.obtenerParametros());
                if (dataSet.Tables.Count > 0)
                {
                    foreach (DataRow fila in dataSet.Tables[0].Rows)
                    {
                        Estudiante e = new Estudiante();
                        string idCarrera;
                        string idUsuario;
                        e.Id = Int32.Parse(fila["id"].ToString());
                        e.Nombre = fila["nombre"].ToString();
                        e.Cedula = fila["cedula"].ToString();
                        e.Email = fila["email"].ToString();
                        //Carrera del estudiante *si tiene*
                        idCarrera = fila["id_carrera"].ToString();
                        e.EstaActivo = fila["esta_activo"].ToString().Equals("S");
                        e.Promedio = Int32.Parse(fila["promedio"].ToString());
                        //Usuario del estudiante *si tiene*
                        idUsuario = fila["id_usuario"].ToString();

                        if (!String.IsNullOrEmpty(idCarrera))
                        {
                            Carrera c = new Carrera();
                            c.Id = Int32.Parse(fila["c_id"].ToString());
                            c.CodigoCarrera = fila["cod_carrera"].ToString();
                            c.Descripcion = fila["descripcion"].ToString();
                            //Le asigna la carrera
                            e.Carrera = c;
                        }

                        if (!String.IsNullOrEmpty(idUsuario))
                        {
                            Usuario u = new Usuario();
                            u.Id = Int32.Parse(fila["u_id"].ToString());
                            u.NombreUsuario = fila["nombre_usuario"].ToString();
                            u.Contrasena = Seguridad.Desencriptar(fila["contrasena"].ToString());
                            u.EsAdmin = fila["es_admin"].ToString().Equals("S");
                            //Le asigna el usuario
                            e.Usuario = u;
                        }
                        //lo agrega a la lista
                        estudiantes.Add(e);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return estudiantes;
        }
        /// <summary>
        /// Crea un nuevo objeto Estudiante en la base de datos
        /// </summary>
        /// <param name="oEstudiante">Entidad Estudiante</param>
        /// <returns>True = Creado, False = No creado</returns>
        public bool CrearEstudiante(Estudiante oEstudiante)
        {
            bool creado = false;
            try
            {
                AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
                Parametro oParametro = new Parametro();

                string sql = "INSERT INTO " + cnx.Schema + ".estudiante" +
                    " (nombre, cedula, email, id_carrera, esta_activo, promedio, id_usuario)" +
                    " VALUES (@nombre, @cedula, @email, @id_carrera, @esta_activo, @promedio, @id_usuario);";

                oParametro.agregarParametro("@nombre", NpgsqlDbType.Varchar, oEstudiante.Nombre);
                oParametro.agregarParametro("@cedula", NpgsqlDbType.Varchar, oEstudiante.Cedula);
                oParametro.agregarParametro("@email", NpgsqlDbType.Varchar, oEstudiante.Email);
                //Si el id de la carrera no está nulo se lo pasa, si no, le pasa un nulo
                if (oEstudiante.Carrera?.Id > 0)
                {
                    oParametro.agregarParametro("@id_carrera", NpgsqlDbType.Integer, oEstudiante.Carrera.Id);
                }
                else
                {
                    oParametro.agregarParametro("@id_carrera", NpgsqlDbType.Integer, DBNull.Value);
                }
                oParametro.agregarParametro("@esta_activo", NpgsqlDbType.Varchar, (oEstudiante.EstaActivo ? "S" : "N"));
                oParametro.agregarParametro("@promedio", NpgsqlDbType.Double, oEstudiante.Promedio);
                //Si el id del usuario no está nulo se lo pasa, si no, le pasa un nulo
                if (oEstudiante.Usuario?.Id > 0)
                {
                    oParametro.agregarParametro("@id_usuario", NpgsqlDbType.Integer, oEstudiante.Usuario.Id);
                }
                else
                {
                    oParametro.agregarParametro("@id_usuario", NpgsqlDbType.Integer, DBNull.Value);
                }

                cnx.ejecutarSQL(sql, oParametro.obtenerParametros());
                creado = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return creado;
        }
        /// <summary>
        /// Actualiza un objeto Estudiante en la base de datos 
        /// </summary>
        /// <param name="oEstudiante">Entidad Estudiante</param>
        /// <returns>True = Actualizado, False = No actualizado</returns>
        public bool ActualizarEstudiante(Estudiante oEstudiante)
        {
            bool actualizado = false;
            try
            {
                AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
                Parametro oParametro = new Parametro();

                string sql = "UPDATE " + cnx.Schema + ".estudiante" +
                    " SET nombre = @nombre, cedula = @cedula, email = @email, id_carrera = @id_carrera," +
                    " esta_activo = @esta_activo, promedio = @promedio, id_usuario = @id_usuario" +
                    " WHERE id = @id;";

                oParametro.agregarParametro("@nombre", NpgsqlDbType.Varchar, oEstudiante.Nombre);
                oParametro.agregarParametro("@cedula", NpgsqlDbType.Varchar, oEstudiante.Cedula);
                oParametro.agregarParametro("@email", NpgsqlDbType.Varchar, oEstudiante.Email);
                //Si el id de la carrera no está nulo se lo pasa, si no, le pasa un nulo
                if (oEstudiante.Carrera?.Id > 0)
                {
                    oParametro.agregarParametro("@id_carrera", NpgsqlDbType.Integer, oEstudiante.Carrera.Id);
                }
                else
                {
                    oParametro.agregarParametro("@id_carrera", NpgsqlDbType.Integer, DBNull.Value);
                }
                oParametro.agregarParametro("@esta_activo", NpgsqlDbType.Varchar, (oEstudiante.EstaActivo ? "S" : "N"));
                oParametro.agregarParametro("@promedio", NpgsqlDbType.Double, oEstudiante.Promedio);
                //Si el id del usuario no está nulo se lo pasa, si no, le pasa un nulo
                if (oEstudiante.Usuario?.Id > 0 )
                {
                    oParametro.agregarParametro("@id_usuario", NpgsqlDbType.Integer, oEstudiante.Usuario.Id);
                }
                else
                {
                    oParametro.agregarParametro("@id_usuario", NpgsqlDbType.Integer, DBNull.Value);
                }
                oParametro.agregarParametro("@id", NpgsqlDbType.Integer, oEstudiante.Id);

                cnx.ejecutarSQL(sql, oParametro.obtenerParametros());
                actualizado = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return actualizado;
        }
        /// <summary>
        /// Elimina un objeto Estudiante en la base de datos
        /// </summary>
        /// <param name="oEstudiante">Entidad Estudiante</param>
        /// <returns>True = Eliminado, False = No eliminado</returns>
        public bool EliminarEstudiante(Estudiante oEstudiante)
        {
            bool eliminado = false;
            try
            {
                AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
                Parametro oParametro = new Parametro();

                string sql = "DELETE FROM " + cnx.Schema + ".estudiante" +
                    " WHERE id = @id;";
                                 
                oParametro.agregarParametro("@id", NpgsqlDbType.Integer, oEstudiante.Id);

                cnx.ejecutarSQL(sql, oParametro.obtenerParametros());
                eliminado = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return eliminado;
        }
    }
}
