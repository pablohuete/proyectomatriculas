﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sistemaMatricula.Repositorios.Datos;
using sistemaMatricula.Entidades;
using System.Data;

namespace sistemaMatricula.Repositorios
{
    public static class RepParametroSistema
    {
        public static ParametroSistema ObtenerParametroSistema()
        {
            ParametroSistema oParametroSistema = new ParametroSistema();
            try
            {
                AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
                DataSet dataSet;
                string sql = "SELECT * FROM " + cnx.Schema + ".parametros WHERE " +
                    " id IN(SELECT MAX(id) AS ultima_fila FROM " + cnx.Schema + ".parametros);";
                
                dataSet = cnx.ejecutarConsultaSQL(sql);

                if (dataSet.Tables[0].Rows.Count > 0)
                {
                    DataRow fila = dataSet.Tables[0].Rows[0];
                    oParametroSistema.Id = Int32.Parse(fila["id"].ToString());
                    oParametroSistema.CostoCredito = Double.Parse(fila["costo_credito"].ToString());
                    oParametroSistema.CostoSeguro = Double.Parse(fila["costo_seguro"].ToString());

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return oParametroSistema;
        }
    }
}
