﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using NpgsqlTypes;
using sistemaMatricula.Repositorios.Datos;
using sistemaMatricula.Entidades;

namespace sistemaMatricula.Repositorios
{
    public class RepPeriodo
    {
        /// <summary>
        /// Obtiene una lista con los perídos
        /// </summary>
        /// <returns>Arreglo de Periodos</returns>
        public List<Periodo> ObtenerPeriodos()
        {
            List<Periodo> periodos = new List<Periodo>();

            AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
            DataSet dataSet;
            try
            {
                string sql = "SELECT * FROM " + cnx.Schema + ".periodo ORDER BY id ASC";

                dataSet = cnx.ejecutarConsultaSQL(sql);
                if (dataSet.Tables.Count > 0)
                {
                    foreach (DataRow fila in dataSet.Tables[0].Rows)
                    {
                        periodos.Add(new Periodo
                        {
                            Id = Int32.Parse(fila["id"].ToString()),
                            Descripcion = fila["descripcion"].ToString(),
                            FechaInicio = DateTime.Parse(fila["fecha_inicio"].ToString()),
                            FechaFin = DateTime.Parse(fila["fecha_fin"].ToString()),
                            Activo = fila["activo"].ToString().Equals("S")
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return periodos;
        }
        /// <summary>
        /// Busca períodos en la base de datos
        /// </summary>
        /// <param name="oPeriodo">Entidad Periodo</param>
        /// <returns>Lista de periodos filtrada</returns>
        public List<Periodo> BuscarPeriodos(Periodo oPeriodo)
        {
            AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
            Parametro oParametro = new Parametro();
            DataSet dataSet;
            List<Periodo> periodos = new List<Periodo>();
            try
            {
                string sql = "SELECT * FROM " + cnx.Schema + ".periodo" +
                    " WHERE descripcion LIKE @descripcion";

                oParametro.agregarParametro("@descripcion", NpgsqlDbType.Varchar, "%" + oPeriodo.Descripcion + "%");
                if (!String.IsNullOrEmpty(oPeriodo.FechaInicio.ToString()))
                {
                    sql += " AND fecha_inicio >= @fecha_inicio";
                    oParametro.agregarParametro("@fecha_inicio", NpgsqlDbType.Date, oPeriodo.FechaInicio);                    
                }
                if (!String.IsNullOrEmpty(oPeriodo.FechaFin.ToString()))
                {
                    sql += " AND fecha_fin <= @fecha_fin";
                    oParametro.agregarParametro("@fecha_fin", NpgsqlDbType.Date, oPeriodo.FechaFin);
                }
                string activo = "N";                                                                
                if (oPeriodo.Activo)
                {
                    sql += " AND activo = @activo";
                    activo = "S";
                    oParametro.agregarParametro("@activo", NpgsqlDbType.Varchar, activo);
                }
                
                dataSet = cnx.ejecutarConsultaSQL(sql, "periodo", oParametro.obtenerParametros());
                if (dataSet.Tables.Count > 0)
                {
                    foreach (DataRow fila in dataSet.Tables[0].Rows)
                    {
                        periodos.Add(new Periodo
                        {
                            Id = Int32.Parse(fila["id"].ToString()),
                            Descripcion = fila["descripcion"].ToString(),
                            FechaInicio = DateTime.Parse(fila["fecha_inicio"].ToString()),
                            FechaFin = DateTime.Parse(fila["fecha_fin"].ToString()),
                            Activo = fila["activo"].ToString().Equals("S")                          
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return periodos;
        }
        /// <summary>
        /// Guarda un Objeto Periodo en la base de datos
        /// </summary>
        /// <param name="oPeriodo">Entidad Periodo a Crear</param>
        /// <returns>True = Creado, False = No creado</returns>
        public bool CrearPeriodo(Periodo oPeriodo)
        {
            bool creado = false;
            try
            {
                AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
                Parametro oParametro = new Parametro();
                string sql = "INSERT INTO " + cnx.Schema + ".periodo (descripcion, fecha_inicio, fecha_fin, activo)" +
                    " VALUES (@descripcion, @fecha_inicio, @fecha_fin, @activo);";

                oParametro.agregarParametro("@descripcion", NpgsqlDbType.Varchar, oPeriodo.Descripcion);
                oParametro.agregarParametro("@fecha_inicio", NpgsqlDbType.Date, oPeriodo.FechaInicio);
                oParametro.agregarParametro("@fecha_fin", NpgsqlDbType.Date, oPeriodo.FechaFin);
                string activo = "N";
                if (oPeriodo.Activo)
                {
                    activo = "S";
                }
                oParametro.agregarParametro("@activo", NpgsqlDbType.Varchar, activo);

                cnx.ejecutarSQL(sql, oParametro.obtenerParametros());
                creado = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return creado;
        }
        /// <summary>
        /// Actualiza un objeto período en la base de datos
        /// </summary>
        /// <param name="oPeriodo">Entidad Aula</param>
        /// <returns>True = Actualizado, False = No actualizado</returns>
        public bool ActualizarPeriodo(Periodo oPeriodo)
        {
            bool actualizado = false;
            try
            {
                AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
                Parametro oParametro = new Parametro();
                string sql = "UPDATE " + cnx.Schema + ".periodo" +
                    " SET descripcion = @descripcion, fecha_inicio = @fecha_inicio, fecha_fin = @fecha_fin, activo = @activo" +
                    " WHERE id = @id;";

                oParametro.agregarParametro("@descripcion", NpgsqlDbType.Varchar, oPeriodo.Descripcion);
                oParametro.agregarParametro("@fecha_inicio", NpgsqlDbType.Date, oPeriodo.FechaInicio);
                oParametro.agregarParametro("@fecha_fin", NpgsqlDbType.Date, oPeriodo.FechaFin);
                string activo = "N";
                if (oPeriodo.Activo)
                {
                    activo = "S";
                }
                oParametro.agregarParametro("@activo", NpgsqlDbType.Varchar, activo);
                oParametro.agregarParametro("@id", NpgsqlDbType.Integer, oPeriodo.Id);

                cnx.ejecutarSQL(sql, oParametro.obtenerParametros());
                actualizado = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return actualizado;
        }
        /// <summary>
        /// Elimina un objeto Período en la base de datos
        /// </summary>
        /// <param name="oPeriodo">Entidad Periodo</param>
        /// <returns>True = Eliminado, False = No eliminado</returns>
        public bool EliminarPeriodo(Periodo oPeriodo)
        {
            AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
            Parametro oParametro = new Parametro();
            try
            {
                string sql = "DELETE FROM " + cnx.Schema + ".periodo" + " WHERE id = @id";

                oParametro.agregarParametro("@id", NpgsqlDbType.Integer, oPeriodo.Id);

                cnx.ejecutarSQL(sql, oParametro.obtenerParametros());
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
