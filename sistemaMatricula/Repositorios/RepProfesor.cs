﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sistemaMatricula.Entidades;
using System.Data;
using NpgsqlTypes;
using sistemaMatricula.Repositorios.Datos;

namespace sistemaMatricula.Repositorios
{
    public class RepProfesor
    {
        /// <summary>
        /// Consulta los profesores guardados en la base de datos
        /// </summary>
        /// <returns>Lista de Profesores</returns>
        public List<Profesor> CargarProfesores()
        {
            List<Profesor> profesores = new List<Profesor>();
            try
            {
                AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
                DataSet dataSet;

                string sql = "SELECT P.*, C.id AS c_id, C.cod_carrera, C.descripcion," +
                    " U.id AS u_id, U.nombre_usuario, U.contrasena, U.es_admin" +
                    " FROM " + cnx.Schema + ".profesor P" +
                    " LEFT JOIN " + cnx.Schema + ".carrera C ON(P.id_carrera = C.id)" +
                    " LEFT JOIN " + cnx.Schema + ".usuario U ON(P.id_usuario = U.id)" +
                    " ORDER BY P.nombre ASC;";

                dataSet = cnx.ejecutarConsultaSQL(sql);
                if (dataSet.Tables.Count > 0)
                {
                    foreach (DataRow fila in dataSet.Tables[0].Rows)
                    {
                        Profesor p = new Profesor();
                        string idCarrera;
                        string idUsuario;
                        p.Id = Int32.Parse(fila["id"].ToString());
                        p.Nombre = fila["nombre"].ToString();
                        p.Cedula = fila["cedula"].ToString();
                        p.Email = fila["email"].ToString();
                        //Carrera del profesor *si tiene*
                        idCarrera = fila["id_carrera"].ToString();
                        //p.EstaActivo = fila["esta_activo"].ToString().Equals("S");
                        
                        //Usuario del profesor *si tiene*
                        idUsuario = fila["id_usuario"].ToString();

                        if (!String.IsNullOrEmpty(idCarrera))
                        {
                            Carrera c = new Carrera();
                            c.Id = Int32.Parse(fila["c_id"].ToString());
                            c.CodigoCarrera = fila["cod_carrera"].ToString();
                            c.Descripcion = fila["descripcion"].ToString();
                            //Le asigna la carrera
                            p.Carrera = c;
                        }

                        if (!String.IsNullOrEmpty(idUsuario))
                        {
                            Usuario u = new Usuario();
                            u.Id = Int32.Parse(fila["u_id"].ToString());
                            u.NombreUsuario = fila["nombre_usuario"].ToString();
                            u.Contrasena = Seguridad.Desencriptar(fila["contrasena"].ToString());
                            u.EsAdmin = fila["es_admin"].ToString().Equals("S");
                            //Le asigna el usuario
                            p.Usuario = u;
                        }
                        //lo agrega a la lista
                        profesores.Add(p);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return profesores;
        }
        /// <summary>
        /// Realiza una consulta filtrada de los profesores guardados en la base de datos
        /// </summary>
        /// <param name="oProfesor">Entidad Profesor</param>
        /// <returns>Lista filtrada de Profesores</returns>
        public List<Profesor> CargarProfesores(Profesor oProfesor)
        {
            List<Profesor> profesores = new List<Profesor>();
            try
            {
                AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
                Parametro oParametro = new Parametro();
                DataSet dataSet;

                string sql = "SELECT P.*, C.id AS c_id, C.cod_carrera, C.descripcion," +
                    " U.id AS u_id, U.nombre_usuario, U.contrasena, U.es_admin" +
                    " FROM " + cnx.Schema + ".profesor P" +
                    " LEFT JOIN " + cnx.Schema + ".carrera C ON(P.id_carrera = C.id)" +
                    " LEFT JOIN " + cnx.Schema + ".usuario U ON(P.id_usuario = U.id)" +
                    " WHERE P.cedula LIKE @cedula" + //filtro por cédula
                    " ORDER BY P.nombre ASC;";

                oParametro.agregarParametro("@cedula", NpgsqlDbType.Varchar, "%" + oProfesor.Cedula + "%");
                dataSet = cnx.ejecutarConsultaSQL(sql, "profesor", oParametro.obtenerParametros());
                if (dataSet.Tables.Count > 0)
                {
                    foreach (DataRow fila in dataSet.Tables[0].Rows)
                    {
                        Profesor p = new Profesor();
                        string idCarrera;
                        string idUsuario;
                        p.Id = Int32.Parse(fila["id"].ToString());
                        p.Nombre = fila["nombre"].ToString();
                        p.Cedula = fila["cedula"].ToString();
                        p.Email = fila["email"].ToString();
                        //Carrera del profesor *si tiene*
                        idCarrera = fila["id_carrera"].ToString();
                        //p.EstaActivo = fila["esta_activo"].ToString().Equals("S");
                        //p.Promedio = Int32.Parse(fila["promedio"].ToString());
                        //Usuario del profesor *si tiene*
                        idUsuario = fila["id_usuario"].ToString();

                        if (!String.IsNullOrEmpty(idCarrera))
                        {
                            Carrera c = new Carrera();
                            c.Id = Int32.Parse(fila["c_id"].ToString());
                            c.CodigoCarrera = fila["cod_carrera"].ToString();
                            c.Descripcion = fila["descripcion"].ToString();
                            //Le asigna la carrera
                            p.Carrera = c;
                        }

                        if (!String.IsNullOrEmpty(idUsuario))
                        {
                            Usuario u = new Usuario();
                            u.Id = Int32.Parse(fila["u_id"].ToString());
                            u.NombreUsuario = fila["nombre_usuario"].ToString();
                            u.Contrasena = Seguridad.Desencriptar(fila["contrasena"].ToString());
                            u.EsAdmin = fila["es_admin"].ToString().Equals("S");
                            //Le asigna el usuario
                            p.Usuario = u;
                        }
                        //lo agrega a la lista
                        profesores.Add(p);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return profesores;
        }
        /// <summary>
        /// Crea un nuevo objeto Profesor en la base de datos
        /// </summary>
        /// <param name="oProfesor">Entidad Profesor</param>
        /// <returns>True = Creado, False = No creado</returns>
        public bool CrearProfesor(Profesor oProfesor)
        {
            bool creado = false;
            try
            {
                AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
                Parametro oParametro = new Parametro();

                string sql = "INSERT INTO " + cnx.Schema + ".profesor" +
                    " (nombre, cedula, email, id_carrera, id_usuario)" +
                    " VALUES (@nombre, @cedula, @email, @id_carrera, @id_usuario);";

                oParametro.agregarParametro("@nombre", NpgsqlDbType.Varchar, oProfesor.Nombre);
                oParametro.agregarParametro("@cedula", NpgsqlDbType.Varchar, oProfesor.Cedula);
                oParametro.agregarParametro("@email", NpgsqlDbType.Varchar, oProfesor.Email);
                //Si el id de la carrera no está nulo se lo pasa, si no, le pasa un nulo
                if (oProfesor.Carrera?.Id > 0)
                {
                    oParametro.agregarParametro("@id_carrera", NpgsqlDbType.Integer, oProfesor.Carrera.Id);
                }
                else
                {
                    oParametro.agregarParametro("@id_carrera", NpgsqlDbType.Integer, DBNull.Value);
                }
                //oParametro.agregarParametro("@esta_activo", NpgsqlDbType.Varchar, (oProfesor.EstaActivo ? "S" : "N"));
                //oParametro.agregarParametro("@promedio", NpgsqlDbType.Double, oProfesor.Promedio);
                //Si el id del usuario no está nulo se lo pasa, si no, le pasa un nulo
                if (oProfesor.Usuario?.Id > 0)
                {
                    oParametro.agregarParametro("@id_usuario", NpgsqlDbType.Integer, oProfesor.Usuario.Id);
                }
                else
                {
                    oParametro.agregarParametro("@id_usuario", NpgsqlDbType.Integer, DBNull.Value);
                }

                cnx.ejecutarSQL(sql, oParametro.obtenerParametros());
                creado = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return creado;
        }
        /// <summary>
        /// Actualiza un objeto Profesor en la base de datos 
        /// </summary>
        /// <param name="oProfesor">Entidad Profesor</param>
        /// <returns>True = Actualizado, False = No actualizado</returns>
        public bool ActualizarProfesor(Profesor oProfesor)
        {
            bool actualizado = false;
            try
            {
                AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
                Parametro oParametro = new Parametro();

                string sql = "UPDATE " + cnx.Schema + ".profesor" +
                    " SET nombre = @nombre, cedula = @cedula, email = @email, id_carrera = @id_carrera," +
                    " id_usuario = @id_usuario" +
                    " WHERE id = @id;";

                oParametro.agregarParametro("@nombre", NpgsqlDbType.Varchar, oProfesor.Nombre);
                oParametro.agregarParametro("@cedula", NpgsqlDbType.Varchar, oProfesor.Cedula);
                oParametro.agregarParametro("@email", NpgsqlDbType.Varchar, oProfesor.Email);
                //Si el id de la carrera no está nulo se lo pasa, si no, le pasa un nulo
                if (oProfesor.Carrera?.Id > 0)
                {
                    oParametro.agregarParametro("@id_carrera", NpgsqlDbType.Integer, oProfesor.Carrera.Id);
                }
                else
                {
                    oParametro.agregarParametro("@id_carrera", NpgsqlDbType.Integer, DBNull.Value);
                }
                //oParametro.agregarParametro("@esta_activo", NpgsqlDbType.Varchar, (oProfesor.EstaActivo ? "S" : "N"));
                //oParametro.agregarParametro("@promedio", NpgsqlDbType.Double, oProfesor.Promedio);
                //Si el id del usuario no está nulo se lo pasa, si no, le pasa un nulo
                if (oProfesor.Usuario?.Id > 0)
                {
                    oParametro.agregarParametro("@id_usuario", NpgsqlDbType.Integer, oProfesor.Usuario.Id);
                }
                else
                {
                    oParametro.agregarParametro("@id_usuario", NpgsqlDbType.Integer, DBNull.Value);
                }
                oParametro.agregarParametro("@id", NpgsqlDbType.Integer, oProfesor.Id);

                cnx.ejecutarSQL(sql, oParametro.obtenerParametros());
                actualizado = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return actualizado;
        }
        /// <summary>
        /// Elimina un objeto Profesor en la base de datos
        /// </summary>
        /// <param name="oProfesor">Entidad Profesor</param>
        /// <returns>True = Eliminado, False = No eliminado</returns>
        public bool EliminarProfesor(Profesor oProfesor)
        {
            bool eliminado = false;
            try
            {
                AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
                Parametro oParametro = new Parametro();

                string sql = "DELETE FROM " + cnx.Schema + ".profesor" +
                    " WHERE id = @id;";

                oParametro.agregarParametro("@id", NpgsqlDbType.Integer, oProfesor.Id);

                cnx.ejecutarSQL(sql, oParametro.obtenerParametros());
                eliminado = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return eliminado;
        }
    }
}
