﻿using sistemaMatricula.Repositorios.Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using NpgsqlTypes;

namespace sistemaMatricula.Repositorios
{
    public class RepUsuario
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="oUsuario"></param>
        /// <returns></returns>
        public Entidades.Usuario Login(Entidades.Usuario oUsuario)
        {
            AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
            try
            {
                string sql = "SELECT id, nombre_usuario, contrasena, es_admin FROM " +
                    cnx.Schema + ".usuario" +
                    " WHERE nombre_usuario = @usuario AND contrasena = @password";
                #region
                //Seguridad de la clave
                string contrasenaEncriptada = Seguridad.Encriptar(oUsuario.Contrasena);
                #endregion
                Parametro oParametro = new Parametro();
                oParametro.agregarParametro("@usuario", NpgsqlDbType.Varchar, oUsuario.NombreUsuario);
                //oParametro.agregarParametro("@password", NpgsqlDbType.Varchar, oUsuario.Contrasena);
                oParametro.agregarParametro("@password", NpgsqlDbType.Varchar, contrasenaEncriptada);

                DataSet dataSet = cnx.ejecutarConsultaSQL(sql, "usuario", oParametro.obtenerParametros());

                if (dataSet.Tables[0].Rows.Count > 0)
                {
                    bool esAdmin = false;

                    DataRow fila = dataSet.Tables[0].Rows[0];
                    oUsuario.Id = Int32.Parse(fila["id"].ToString());
                    oUsuario.NombreUsuario = fila["nombre_usuario"].ToString();
                    //Desencriptar la clave
                    oUsuario.Contrasena = Seguridad.Desencriptar(fila["contrasena"].ToString());
                    //oUsuario.Contrasena = fila["contrasena"].ToString();
                    if (fila["es_admin"].ToString() == "S")
                    {
                        esAdmin = true;
                    }
                    oUsuario.EsAdmin = esAdmin;
                }
                else
                {
                    throw new Exception("Usuario no encontrado");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return oUsuario;
        }
        /// <summary>
        /// Obtiene una lista de Usuarios de base de datos
        /// </summary>
        /// <returns>Arreglo de Usuarios</returns>
        public List<Entidades.Usuario> ObtenerUsuarios()
        {
            List<Entidades.Usuario> usuarios = new List<Entidades.Usuario>();
            try
            {
                AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
                DataSet dataSet;
                string sql = "SELECT * FROM " + cnx.Schema + ".usuario ORDER BY id ASC";

                dataSet = cnx.ejecutarConsultaSQL(sql);
                if (dataSet.Tables.Count > 0)
                {
                    foreach (DataRow fila in dataSet.Tables[0].Rows)
                    {
                        usuarios.Add(new Entidades.Usuario
                        {
                            Id = Int32.Parse(fila["id"].ToString()),
                            NombreUsuario = fila["nombre_usuario"].ToString(),
                            Contrasena = fila["contrasena"].ToString(),
                            EsAdmin = (fila["es_admin"].ToString() == "S")
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return usuarios;
        }
        /// <summary>
        /// Realiza una consulta a la base de datos en la tabla usuario para hacer una busqueda otraer todos los datos
        /// </summary>
        /// <param name="filtro">String para la búsqueda</param>
        /// <returns>Lista con los usuarios</returns>
        public List<Entidades.Usuario> CargarUsuarios(string filtro)
        {
            List<Entidades.Usuario> usuarios = new List<Entidades.Usuario>();
            AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
            try
            {
                DataSet dataSet;
                string sql = "SELECT id, nombre_usuario, contrasena, es_admin FROM " + cnx.Schema + ".usuario " +
                    "ORDER BY nombre ASC";

                if (!String.IsNullOrEmpty(filtro))
                {
                    Parametro oParametros = new Parametro();
                    sql = "SELECT id, nombre_usuario, contrasena, es_admin" +
                        " FROM " + cnx.Schema + ".usuario" +
                        " WHERE nombre_usuario like lower(@filtro)";

                    oParametros.agregarParametro("@filtro", NpgsqlDbType.Varchar, "%" + filtro + "%");
                    dataSet = cnx.ejecutarConsultaSQL(sql, "usuario", oParametros.obtenerParametros());
                }
                else
                {
                    dataSet = cnx.ejecutarConsultaSQL(sql);
                }
                if (dataSet.Tables.Count > 0)
                {
                    foreach (DataRow fila in dataSet.Tables[0].Rows)
                    {
                        usuarios.Add(new Entidades.Usuario
                        {
                            Id = Int32.Parse(fila["id"].ToString()),
                            NombreUsuario = fila["nombre_usuario"].ToString(),
                            Contrasena = fila["contrasena"].ToString(),
                            EsAdmin = (fila["es_admin"].ToString() == "S")
                        });
                    }
                }                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return usuarios;
        }
        /// <summary>
        /// Crea un nuevo usuario en la base de datos
        /// </summary>
        /// <param name="oUsuario">Entidad Usuario</param>
        /// <returns>True = Creado, False = No creado</returns>
        public bool CrearNuevo(Entidades.Usuario oUsuario)
        {
            bool creado = false;
            try
            {
                AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
                Parametro oParametro = new Parametro();
                string sql = "INSERT INTO " + cnx.Schema + ".usuario (nombre_usuario, contrasena, es_admin)" +
                    " VALUES (@nombre_usuario, @contrasena, @es_admin);";

                oParametro.agregarParametro("@nombre_usuario", NpgsqlDbType.Varchar, oUsuario.NombreUsuario);
                oParametro.agregarParametro("@contrasena", NpgsqlDbType.Varchar, Seguridad.Encriptar(oUsuario.Contrasena));
                string esAdmin = "N";
                if (oUsuario.EsAdmin)
                {
                    esAdmin = "S";
                }
                oParametro.agregarParametro("@es_admin", NpgsqlDbType.Varchar, esAdmin);

                cnx.ejecutarSQL(sql, oParametro.obtenerParametros());
                creado = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return creado;
        }
        /// <summary>
        /// Actualiza un objeto aula en la base de datos
        /// </summary>
        /// <param name="oUsuario">Entidad Usuario</param>
        /// <returns>True = Actualizado, False = No actualizado</returns>
        public bool ActualizarUsuario(Entidades.Usuario oUsuario)
        {
            bool actualizado = false;
            try
            {
                AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
                Parametro oParametro = new Parametro();
                string sql = "UPDATE " + cnx.Schema + ".usuario" +
                    " SET nombre_usuario = @nombre_usuario, contrasena = @contrasena, es_admin = @es_admin" +
                    " WHERE id = @id;";

                oParametro.agregarParametro("@nombre_usuario", NpgsqlDbType.Varchar, oUsuario.NombreUsuario);
                oParametro.agregarParametro("@contrasena", NpgsqlDbType.Varchar, Seguridad.Encriptar(oUsuario.Contrasena));
                string esAdmin = "N";
                if (oUsuario.EsAdmin)
                {
                    esAdmin = "S";
                }
                oParametro.agregarParametro("@es_admin", NpgsqlDbType.Varchar, esAdmin);
                oParametro.agregarParametro("@id", NpgsqlDbType.Integer, oUsuario.Id);

                cnx.ejecutarSQL(sql, oParametro.obtenerParametros());
                actualizado = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return actualizado;
        }
        /// <summary>
        /// Elimina un objeto Usuario en la base de datos
        /// </summary>
        /// <param name="oUsuario">Entidad Usuario</param>
        /// <returns>True = Eliminado, False = No eliminado</returns>
        public bool EliminarUsuario(Entidades.Usuario oUsuario)
        {
            bool eliminado = false;
            try
            {
                AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
                Parametro oParametro = new Parametro();
                string sql = "DELETE FROM " + cnx.Schema + ".usuario" +
                    " WHERE id = @id";

                oParametro.agregarParametro("@id", NpgsqlDbType.Integer, oUsuario.Id);

                cnx.ejecutarSQL(sql, oParametro.obtenerParametros());
                eliminado = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return eliminado;
        }
    }
}
